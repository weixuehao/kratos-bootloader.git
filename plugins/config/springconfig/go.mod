module gitee.com/weixuehao/kratos-bootloader/plugins/config/springconfig

go 1.21

toolchain go1.22.1

require github.com/go-kratos/kratos/v2 v2.8.2

require (
	dario.cat/mergo v1.0.0 // indirect
	github.com/kr/text v0.2.0 // indirect
	google.golang.org/protobuf v1.33.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
