package springconfig

import (
	"errors"
	"fmt"
	"github.com/go-kratos/kratos/v2/config"
	"io"
	"net/http"
	"path/filepath"
	"strings"
)

type Option func(*options)

type options struct {
	profile  string
	label    string
	endpoint string
	fileName string
}

// WithLabel With SpringCloud config label .
func WithLabel(label string) Option {
	return func(o *options) {
		o.label = label
	}
}

// WithProfile With SpringCloud config profile .
func WithProfile(profile string) Option {
	return func(o *options) {
		o.profile = profile
	}
}

// WithFileName With SpringCloud config fileName .
func WithFileName(fileName string) Option {
	return func(o *options) {
		o.fileName = fileName
	}
}

// WithEndpoint with SpringCloud config server addr
func WithEndpoint(endpoint string) Option {
	return func(o *options) {
		o.endpoint = endpoint
	}
}

type source struct {
	opts *options
	url  string
}

func NewConfigSource(endpoint string, opts ...Option) (config.Source, error) {
	op := options{}
	for _, o := range opts {
		o(&op)
	}

	if endpoint == "" {
		return nil, errors.New("endpoint invalid")
	}

	if op.fileName == "" {
		return nil, errors.New("fileName invalid")
	}

	if op.profile != "" {
		op.fileName = fmt.Sprintf("%s-%s%s", strings.TrimSuffix(op.fileName, filepath.Ext(op.fileName)), op.profile, filepath.Ext(op.fileName))
	}

	url := ""
	if op.label == "" {
		url = fmt.Sprintf("%s/%s", endpoint, op.fileName)
	} else {
		url = fmt.Sprintf("%s/%s/%s", endpoint, op.label, op.fileName)
	}

	return &source{url: url, opts: &op}, nil
}

// Load return the config values
func (s *source) Load() ([]*config.KeyValue, error) {
	resp, err := http.Get(s.url)
	defer func() {
		if resp != nil {
			_ = resp.Body.Close()
		}
	}()
	if err != nil {
		fmt.Errorf("fail to get config: %s", err)
		return nil, err
	}
	content, errR := io.ReadAll(resp.Body)
	if errR != nil {
		fmt.Errorf("fail to read io stream: %s", err)
		return nil, err
	}
	k := s.opts.fileName

	return []*config.KeyValue{
		{
			Key:    k,
			Value:  content,
			Format: strings.TrimPrefix(filepath.Ext(k), "."),
		},
	}, nil
}

// Watch return the watcher
func (s *source) Watch() (config.Watcher, error) {
	w, err := NewWatcher()
	if err != nil {
		return nil, err
	}
	return w, nil
}
