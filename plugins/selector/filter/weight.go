package filter

import (
	"context"
	"github.com/go-kratos/kratos/v2/selector"
)

func Weight(weight int64) selector.NodeFilter {
	return func(_ context.Context, nodes []selector.Node) []selector.Node {
		newNodes := make([]selector.Node, 0, len(nodes))
		for _, n := range nodes {
			if *n.InitialWeight() >= weight {
				newNodes = append(newNodes, n)
			}
		}
		return newNodes
	}
}
