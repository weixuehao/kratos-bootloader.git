module gitee.com/weixuehao/kratos-bootloader/plugins/encoding/yml

go 1.21

toolchain go1.22.1

require (
	github.com/go-kratos/kratos/v2 v2.8.2
	gopkg.in/yaml.v3 v3.0.1
)

require github.com/kr/text v0.2.0 // indirect
