package broker

import "go.opentelemetry.io/otel/propagation"

var _ propagation.TextMapCarrier = (*HeaderCarrier)(nil)

type HeaderCarrier struct {
	msg *Message
}

func NewMessageCarrier(msg *Message) HeaderCarrier {
	return HeaderCarrier{msg: msg}
}

// Get returns the value associated with the passed key.
func (c HeaderCarrier) Get(key string) string {
	for k, v := range c.msg.Headers {
		if k == key {
			return string(v)
		}
	}
	return ""
}

// Set stores the key-value pair.
func (c HeaderCarrier) Set(key, val string) {
	c.msg.Headers[key] = []byte(val)
}

// Keys lists the keys stored in this carrier.
func (c HeaderCarrier) Keys() []string {
	out := make([]string, len(c.msg.Headers))
	for k := range c.msg.Headers {
		out = append(out, k)
	}
	return out
}
