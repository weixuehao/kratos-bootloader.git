package broker

import (
	"context"
	"crypto/tls"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/google/uuid"
)

type Opt interface {
	Ctx() context.Context
}

type Any interface{}

type Options struct {
	Addrs     []string
	Secure    bool
	TLSConfig *tls.Config
	Logger    *log.Helper
	Tracings  []TracerOption
	// Other options for implementations of the interface
	// can be stored in a context
	CtxValue context.Context
	Context  context.Context
}

func (o Options) Ctx() context.Context {
	return o.CtxValue
}

type Option func(*Options)

func (o *Options) Apply(opts ...Option) {
	for _, opt := range opts {
		opt(o)
	}
}

func NewOptions() Options {
	opt := Options{
		Addrs:     []string{},
		Secure:    false,
		TLSConfig: nil,
		CtxValue:  context.Background(),
		Context:   context.Background(),
		Tracings:  []TracerOption{},
	}
	return opt
}

func NewOptionsAndApply(opts ...Option) Options {
	opt := NewOptions()
	opt.Apply(opts...)
	return opt
}

func WithOptionContext(ctx context.Context) Option {
	return func(o *Options) {
		o.Context = ctx
	}
}

func OptionContextWithValue(k, v interface{}) Option {
	return func(o *Options) {
		if o.CtxValue == nil {
			o.CtxValue = context.Background()
		}
		o.CtxValue = context.WithValue(o.CtxValue, k, v)
	}
}

// WithAddress set broker address
func WithAddress(addressList ...string) Option {
	return func(o *Options) {
		o.Addrs = addressList
	}
}

// WithLogger set broker Logger
func WithLogger(log *log.Helper) Option {
	return func(o *Options) {
		o.Logger = log
	}
}

// Secure communication with the broker
func WithSecure(b bool) Option {
	return func(o *Options) {
		o.Secure = b
	}
}

// Specify TLS Config
func WithTLSConfig(t *tls.Config) Option {
	return func(o *Options) {
		o.TLSConfig = t
	}
}

func WithTracer(opts ...TracerOption) Option {
	return func(opt *Options) {
		opt.Tracings = append(opt.Tracings, opts...)
	}
}

///////////////////////////////////////////////////////////////////////////////

type PublishOptions struct {
	Async    bool
	CtxValue context.Context
	Context  context.Context
}

func (o PublishOptions) Ctx() context.Context {
	return o.CtxValue
}

type PublishOption func(*PublishOptions)

func (o *PublishOptions) Apply(opts ...PublishOption) {
	for _, opt := range opts {
		opt(o)
	}
}

func NewPublishOptions() PublishOptions {
	opt := PublishOptions{
		Async:    false,
		CtxValue: context.Background(),
		Context:  context.Background(),
	}
	return opt
}

func NewPublishOptionsAndApply(opts ...PublishOption) PublishOptions {
	options := NewPublishOptions()
	options.Apply(opts...)
	return options
}

func WithEnableAsync(async bool) PublishOption {
	return func(o *PublishOptions) {
		o.Async = async
	}
}

func PublishContextWithValue(k, v interface{}) PublishOption {
	return func(o *PublishOptions) {
		if o.CtxValue == nil {
			o.CtxValue = context.Background()
		}
		o.CtxValue = context.WithValue(o.CtxValue, k, v)
	}
}

func WithPublishContext(ctx context.Context) PublishOption {
	return func(o *PublishOptions) {
		o.Context = ctx
	}
}

///////////////////////////////////////////////////////////////////////////////

type SubscribeOptions struct {
	ErrorHandler   Handler
	AutoAck        bool
	Queue          string
	MsgHandleChan  chan Event
	MsgRoutineNums int // 消息并发处理协程数
	CtxValue       context.Context
	Context        context.Context
}

func (o SubscribeOptions) Ctx() context.Context {
	return o.CtxValue
}

type SubscribeOption func(*SubscribeOptions)

func (o *SubscribeOptions) Apply(opts ...SubscribeOption) {
	for _, opt := range opts {
		opt(o)
	}
}

func NewSubscribeOptions() SubscribeOptions {
	opt := SubscribeOptions{
		AutoAck:  true,
		Queue:    uuid.New().String(),
		CtxValue: context.Background(),
		Context:  context.Background(),
	}
	return opt
}

func NewSubscribeOptionsAndApply(opts ...SubscribeOption) SubscribeOptions {
	options := NewSubscribeOptions()
	options.Apply(opts...)
	return options
}

func SubscribeContextWithValue(k, v interface{}) SubscribeOption {
	return func(o *SubscribeOptions) {
		if o.CtxValue == nil {
			o.CtxValue = context.Background()
		}
		o.CtxValue = context.WithValue(o.CtxValue, k, v)
	}
}

func WithDisableAutoAck() SubscribeOption {
	return func(o *SubscribeOptions) {
		o.AutoAck = false
	}
}

func WithQueueName(name string) SubscribeOption {
	return func(o *SubscribeOptions) {
		o.Queue = name
	}
}

// ErrorHandler will catch all broker errors that cant be handled
// in normal way, for example Codec errors
func WithErrorHandler(h Handler) SubscribeOption {
	return func(o *SubscribeOptions) {
		o.ErrorHandler = h
	}
}

func WithMsgHandleChan(handleChan chan Event) SubscribeOption {
	return func(o *SubscribeOptions) {
		o.MsgHandleChan = handleChan
	}
}

func WithMsgRoutineNums(msgHandleRoutineNums int) SubscribeOption {
	return func(o *SubscribeOptions) {
		o.MsgRoutineNums = msgHandleRoutineNums
	}
}

func WithSubscribeContext(ctx context.Context) SubscribeOption {
	return func(o *SubscribeOptions) {
		o.Context = ctx
	}
}
