package saka

import (
	"context"
	"fmt"
	"gitee.com/weixuehao/kratos-bootloader/pkg/threading"
	"gitee.com/weixuehao/kratos-bootloader/plugins/broker"
	"github.com/IBM/sarama"
	"github.com/go-kratos/kratos/v2/log"
	"go.opentelemetry.io/otel/attribute"
	semConv "go.opentelemetry.io/otel/semconv/v1.12.0"
	"go.opentelemetry.io/otel/trace"
	"strconv"
)

type event struct {
	subopt         broker.SubscribeOptions
	topic          string
	err            error
	cg             sarama.ConsumerGroup
	cmsg           *sarama.ConsumerMessage
	msg            *broker.Message
	sess           sarama.ConsumerGroupSession
	ctx            context.Context
	span           trace.Span
	consumerTracer *broker.Tracer
}

func (p *event) Topic() string {
	return p.topic
}

func (p *event) Message() *broker.Message {
	return p.msg
}

func (p *event) Ack(metadata string) error {
	if p.subopt.MsgHandleChan != nil && !p.subopt.AutoAck {
		p.finishConsumerSpan(p.span, p.err)
	}
	if p.subopt.AutoAck {
		return fmt.Errorf("auto ack is enabled, cannot manually ack message (topic: %s, partition: %d, offset: %d)", p.cmsg.Topic, p.cmsg.Partition, p.cmsg.Offset)
	} else {
		p.sess.MarkMessage(p.cmsg, metadata)
		return nil
	}

}

func (p *event) Claims() map[string][]int32 {
	return p.sess.Claims()
}

func (p *event) Commit() {
	p.sess.Commit()
}

func (s *event) Context() context.Context {
	return s.ctx
}

func (p *event) Error() error {
	return p.err
}

func (p *event) finishConsumerSpan(span trace.Span, err error) {
	if p.consumerTracer == nil {
		return
	}

	p.consumerTracer.End(context.Background(), span, err)
}

// consumerGroupHandler is the implementation of sarama.ConsumerGroupHandler.
type consumerGroupHandler struct {
	logger         *log.Helper
	handler        broker.Handler
	subopts        broker.SubscribeOptions
	opts           broker.Options
	cg             sarama.ConsumerGroup
	pool           *threading.GoroutinePool
	consumerTracer *broker.Tracer
}

func (h *consumerGroupHandler) Setup(session sarama.ConsumerGroupSession) error {
	h.logger.Infof("Setup generation %d,memberID %s, claims=%#v", session.GenerationID(), session.MemberID(), session.Claims())
	return nil
}
func (h *consumerGroupHandler) Cleanup(session sarama.ConsumerGroupSession) error {
	h.logger.Infof("Cleaning up for generationID %d", session.GenerationID())
	return nil
}
func (h *consumerGroupHandler) ConsumeClaim(sess sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	h.logger.Infof("ConsumeClaim for topic/partition %s/%d, initialOffset=%d, highWaterMarkOffset=%d", claim.Topic(), claim.Partition(), claim.InitialOffset(), claim.HighWaterMarkOffset())
	defer func() {
		if h.subopts.MsgRoutineNums > 0 {
			h.pool.Shutdown()
		}
		h.logger.Infof("ConsumeClaim done for topic/partition %s/%d", claim.Topic(), claim.Partition())
	}()

	if h.subopts.MsgRoutineNums > 0 {
		h.pool = threading.NewGoroutinePool(h.subopts.MsgRoutineNums)
	}

	for {
		select {
		case cmsg, ok := <-claim.Messages():
			if !ok {
				return nil
			}

			msg := &broker.Message{
				Topic:     cmsg.Topic,
				Key:       string(cmsg.Key),
				Value:     cmsg.Value,
				Partition: cmsg.Partition,
				Offset:    cmsg.Offset,
				Timestamp: cmsg.Timestamp,
				Headers:   make(map[string][]byte),
			}

			for _, header := range cmsg.Headers {
				msg.Headers[string(header.Key)] = header.Value
			}
			ctx, span := h.startConsumerSpan(sess.Context(), msg)
			e := &event{msg: msg, topic: cmsg.Topic, cmsg: cmsg, cg: h.cg, sess: sess, subopt: h.subopts, span: span}

			handle := func() {
				err := h.handler(ctx, e)
				if err == nil && h.subopts.AutoAck {
					sess.MarkMessage(cmsg, "")
				} else if err != nil {
					e.err = err
					if h.subopts.ErrorHandler != nil {
						h.subopts.ErrorHandler(ctx, e)
					} else {
						h.logger.Errorf("[kafka]: subscriber error: %v", err)
					}
				}
				h.finishConsumerSpan(span, err)
			}

			if h.subopts.MsgHandleChan != nil {
				h.subopts.MsgHandleChan <- e
				if h.subopts.AutoAck {
					sess.MarkMessage(cmsg, "")
				}
			} else if h.subopts.MsgRoutineNums > 0 {
				h.pool.Submit(handle)
			} else {
				handle()
			}
		case <-sess.Context().Done():
			return nil
		}
	}
	return nil
}

func (h *consumerGroupHandler) startConsumerSpan(ctx context.Context, msg *broker.Message) (context.Context, trace.Span) {
	if h.subopts.MsgHandleChan != nil && h.subopts.AutoAck {
		return ctx, nil
	}
	if h.consumerTracer == nil {
		return ctx, nil
	}

	carrier := broker.NewMessageCarrier(msg)

	attrs := []attribute.KeyValue{
		semConv.MessagingSystemKey.String("kafka"),
		semConv.MessagingDestinationKindTopic,
		semConv.MessagingDestinationKey.String(msg.Topic),
		semConv.MessagingOperationReceive,
		semConv.MessagingMessageIDKey.String(strconv.FormatInt(msg.Offset, 10)),
		semConv.MessagingKafkaPartitionKey.Int64(int64(msg.Partition)),
	}

	var span trace.Span
	ctx, span = h.consumerTracer.Start(ctx, carrier, attrs...)

	return ctx, span
}

func (h *consumerGroupHandler) finishConsumerSpan(span trace.Span, err error) {
	if h.consumerTracer == nil {
		return
	}

	h.consumerTracer.End(context.Background(), span, err)
}
