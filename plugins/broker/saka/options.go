package saka

import (
	"crypto/tls"
	"gitee.com/weixuehao/kratos-bootloader/plugins/broker"
	"github.com/IBM/sarama"
	"golang.org/x/net/proxy"
	"time"
)

// ################################################Custom config################################
// ***********BrokerOption******************
type configEntriesKey struct{}

func WithConfigEntries(value int) broker.Option {
	return broker.OptionContextWithValue(configEntriesKey{}, value)
}

type replicaAssignmentKey struct{}

func WithReplicaAssignment(value int) broker.Option {
	return broker.OptionContextWithValue(replicaAssignmentKey{}, value)
}

//**********SubscribeOption************
//type msgHandleChan struct {}
//func WithConsumerMsgHandleChan(handleChan chan broker.Event) broker.SubscribeOption {
//	return broker.SubscribeContextWithValue(msgHandleChan{}, handleChan)
//}

// ***********PublishOption************
type enableOneTopicOneWriterKey struct{}

func WithProducerEnableOneTopicOneWriterKey(enableOneTopicOneWriter bool) broker.PublishOption {
	return broker.PublishContextWithValue(enableOneTopicOneWriterKey{}, enableOneTopicOneWriter)
}

//type enableAsync struct {}
//func WithProducerEnableAsync(async bool) broker.PublishOption {
//	return broker.PublishContextWithValue(enableAsync{}, async)
//}

type asyncProduceErrorKey struct{}

func WithProducerAsyncProduceErrorKey(produceError func(broker.Publisher)) broker.PublishOption {
	return broker.PublishContextWithValue(asyncProduceErrorKey{}, produceError)
}

type asyncProduceSuccessKey struct{}

func WithProducerAsyncProduceSuccessKey(produceSuccess func(broker.Publisher)) broker.PublishOption {
	return broker.PublishContextWithValue(asyncProduceSuccessKey{}, produceSuccess)
}

// enable producer transaction
type enableTransactionKey struct{}

func WithProducerEnableTransactionKey(enableTransaction bool) broker.PublishOption {
	return broker.PublishContextWithValue(enableTransactionKey{}, enableTransaction)
}

// ###############################################sarama config######################################
type clusterConfigKey struct{}

// Admin is the namespace for ClusterAdmin properties used by the administrative Kafka client.
type adminRetryMax struct{}
type adminRetryBackoff struct{}
type adminTimeout struct{}

// Net is the namespace for network-level properties used by the Broker, and
// shared by the Client/Producer/Consumer.
type netMaxOpenRequests struct{}
type netDialTimeout struct{}
type netReadTimeout struct{}
type netWriteTimeout struct{}
type netTLSEnable struct{}
type netTLSConfig struct{}
type netSASLEnable struct{}
type netSASLVersion struct{}
type netSASLAuthIdentityKey struct{}
type netSASLHandshakeKey struct{}
type netSASLMechanismKey struct{}
type netSASLUser struct{}
type netSASLPassword struct{}
type netSASLTokenProviderKey struct{}
type netSASLSCRAMClientGeneratorFuncKey struct{}
type netSASLSCRAMAuthzIDKey struct{}
type netKeepAlive struct{}
type netProxyEnable struct{}
type netProxyDialer struct{}

// Metadata is the namespace for metadata management properties used by the
// Clietype t, and shared by the Producer/Consumer.
type metadataRetryMax struct{}
type metadataRetryBackoff struct{}
type metadataRetryBackoffFunc struct{}
type metadataRefreshFrequency struct{}
type metadataFull struct{}
type metadataTimeout struct{}
type metadataAllowAutoTopicCreation struct{}

// Producer is the namespace for configuration related to producing messages,
// used by the Producer.
type producerMaxMessageBytes struct{}
type producerRequiredAcks struct{}
type producerTimeout struct{}
type producerCompression struct{}
type producerCompressionLevel struct{}
type producerPartitioner struct{}
type producerIdempotent struct{}
type producerTransactionRetryBackoffFuncKey struct{}
type producerTransactionRetryBackoffKey struct{}
type producerTransactionRetryMaxKey struct{}
type producerTransactionTimeoutKey struct{}
type producerTransactionIDKey struct{}
type producerReturnSuccesses struct{}
type producerReturnErrors struct{}
type producerFlushBytes struct{}
type producerFlushMessages struct{}
type producerFlushFrequency struct{}
type producerFlushMaxMessages struct{}
type producerRetryMax struct{}
type producerRetryBackoff struct{}
type producerRetryBackoffFunc struct{}
type producerInterceptors struct{}

// Consumer is the namespace for configuration related to consuming messages,
// used by the Consumer.
type consumerGroupSessionTimeout struct{}
type consumerGroupHeartbeatInterval struct{}
type consumerGroupRebalanceStrategy struct{}
type consumerGroupRebalanceGroupStrategies struct{}
type consumerGroupRebalanceTimeout struct{}
type consumerGroupRebalanceRetryMax struct{}
type consumerGroupRebalanceRetryBackoff struct{}
type consumerGroupMemberUserData struct{}
type consumerGroupInstanceIdKey struct{}
type consumerGroupResetInvalidOffsetsKey struct{}
type consumerRetryBackoff struct{}
type consumerRetryBackoffFunc struct{}
type consumerFetchMin struct{}
type consumerFetchDefault struct{}
type consumerFetchMax struct{}
type consumerMaxWaitTime struct{}
type consumerMaxProcessingTime struct{}
type consumerReturnErrors struct{}
type consumerOffsetsAutoCommitEnable struct{}
type consumerOffsetsAutoCommitInterval struct{}
type consumerOffsetsInitial struct{}
type consumerOffsetsRetention struct{}
type consumerOffsetsRetryMax struct{}
type consumerIsolationLevel struct{}
type consumerInterceptors struct{}

type clientID struct{}
type rackID struct{}
type channelBufferSize struct{}
type apiVersionsRequest struct{}
type version struct{}

// WithClusterConfig
func WithClusterConfig(cfg *sarama.Config) broker.Option {
	return broker.OptionContextWithValue(clusterConfigKey{}, cfg)
}

func WithConsumerConfig(cfg *sarama.Config) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(clusterConfigKey{}, cfg)
}

func WithProducerConfig(cfg *sarama.Config) broker.PublishOption {
	return broker.PublishContextWithValue(clusterConfigKey{}, cfg)
}

func WithAdminRetryMax(AdminRetryMax int) broker.Option {
	return broker.OptionContextWithValue(adminRetryMax{}, AdminRetryMax)
}

func WithAdminRetryBackoff(AdminRetryBackoff time.Duration) broker.Option {
	return broker.OptionContextWithValue(adminRetryBackoff{}, AdminRetryBackoff)
}

func WithAdminTimeout(AdminTimeout time.Duration) broker.Option {
	return broker.OptionContextWithValue(adminTimeout{}, AdminTimeout)
}

func WithNetMaxOpenRequests(NetMaxOpenRequests int) broker.Option {
	return broker.OptionContextWithValue(netMaxOpenRequests{}, NetMaxOpenRequests)
}

func WithNetDialTimeout(NetDialTimeout time.Duration) broker.Option {
	return broker.OptionContextWithValue(netDialTimeout{}, NetDialTimeout)
}

func WithNetReadTimeout(NetReadTimeout time.Duration) broker.Option {
	return broker.OptionContextWithValue(netReadTimeout{}, NetReadTimeout)
}

func WithNetWriteTimeout(NetWriteTimeout time.Duration) broker.Option {
	return broker.OptionContextWithValue(netWriteTimeout{}, NetWriteTimeout)
}

func WithNetTLSEnable(NetTLSEnable bool) broker.Option {
	return broker.OptionContextWithValue(netTLSEnable{}, NetTLSEnable)
}

func WithNetTLSConfig(NetTLSConfig *tls.Config) broker.Option {
	return broker.OptionContextWithValue(netTLSConfig{}, NetTLSConfig)
}

func WithNetSASLEnable(NetSASLEnable bool) broker.Option {
	return broker.OptionContextWithValue(netSASLEnable{}, NetSASLEnable)
}

func WithNetSASLMechanism(NetSASLMechanism string) broker.Option {
	return broker.OptionContextWithValue(netSASLMechanismKey{}, NetSASLMechanism)
}

func WithNetSASLVersion(NetSASLVersion int16) broker.Option {
	return broker.OptionContextWithValue(netSASLVersion{}, NetSASLVersion)
}

func WithNetSASLHandshake(NetSASLHandshake bool) broker.Option {
	return broker.OptionContextWithValue(netSASLHandshakeKey{}, NetSASLHandshake)
}

func WithNetSASLAuthIdentity(NetSASLAuthIdentity string) broker.Option {
	return broker.OptionContextWithValue(netSASLAuthIdentityKey{}, NetSASLAuthIdentity)
}

func WithNetSASLUser(NetSASLUser string) broker.Option {
	return broker.OptionContextWithValue(netSASLUser{}, NetSASLUser)
}

func WithNetSASLPassword(NetSASLPassword string) broker.Option {
	return broker.OptionContextWithValue(netSASLPassword{}, NetSASLPassword)
}

func WithNetSASLSCRAMAuthzID(NetSASLSCRAMAuthzID string) broker.Option {
	return broker.OptionContextWithValue(netSASLSCRAMAuthzIDKey{}, NetSASLSCRAMAuthzID)
}

func WithNetSASLSCRAMClientGeneratorFunc(NetSASLSCRAMClientGeneratorFunc func() sarama.SCRAMClient) broker.Option {
	return broker.OptionContextWithValue(netSASLSCRAMClientGeneratorFuncKey{}, NetSASLSCRAMClientGeneratorFunc)
}

// TokenProvider is a user-defined callback for generating
// access tokens for SASL/OAUTHBEARER auth. See the
// AccessTokenProvider interface docs for proper implementation
// guidelines.
func WithNetSASLTokenProvider(NetSASLTokenProvider func() sarama.AccessTokenProvider) broker.Option {
	return broker.OptionContextWithValue(netSASLTokenProviderKey{}, NetSASLTokenProvider)
}

//GSSAPI GSSAPIConfig

func WithNetKeepAlive(NetKeepAlive time.Duration) broker.Option {
	return broker.OptionContextWithValue(netKeepAlive{}, NetKeepAlive)
}

func WithNetProxyEnable(NetProxyEnable bool) broker.Option {
	return broker.OptionContextWithValue(netProxyEnable{}, NetProxyEnable)
}

func WithNetProxyDialer(NetProxyDialer proxy.Dialer) broker.Option {
	return broker.OptionContextWithValue(netProxyDialer{}, NetProxyDialer)
}

func WithMetadataRetryMax(MetadataRetryMax int) broker.Option {
	return broker.OptionContextWithValue(metadataRetryMax{}, MetadataRetryMax)
}

func WithMetadataRetryBackoff(MetadataRetryBackoff time.Duration) broker.Option {
	return broker.OptionContextWithValue(metadataRetryBackoff{}, MetadataRetryBackoff)
}

func WithMetadataRetryBackoffFunc(MetadataRetryBackoffFunc func(retries, maxRetries int) time.Duration) broker.Option {
	return broker.OptionContextWithValue(metadataRetryBackoffFunc{}, MetadataRetryBackoffFunc)
}

func WithMetadataRefreshFrequency(MetadataRefreshFrequency time.Duration) broker.Option {
	return broker.OptionContextWithValue(metadataRefreshFrequency{}, MetadataRefreshFrequency)
}

func WithMetadataFull(MetadataFull bool) broker.Option {
	return broker.OptionContextWithValue(metadataFull{}, MetadataFull)
}

func WithMetadataTimeout(MetadataTimeout time.Duration) broker.Option {
	return broker.OptionContextWithValue(metadataTimeout{}, MetadataTimeout)
}

func WithMetadataAllowAutoTopicCreation(MetadataAllowAutoTopicCreation bool) broker.Option {
	return broker.OptionContextWithValue(metadataAllowAutoTopicCreation{}, MetadataAllowAutoTopicCreation)
}

func WithProducerMaxMessageBytes(ProducerMaxMessageBytes int) broker.PublishOption {
	return broker.PublishContextWithValue(producerMaxMessageBytes{}, ProducerMaxMessageBytes)
}

// value: （0：NoResponse）、（1：WaitForLocal）、（-1：WaitForAll）
func WithProducerRequiredAcks(ProducerRequiredAcks int16) broker.PublishOption {
	return broker.PublishContextWithValue(producerRequiredAcks{}, ProducerRequiredAcks)
}

func WithProducerTimeout(ProducerTimeout time.Duration) broker.PublishOption {
	return broker.PublishContextWithValue(producerTimeout{}, ProducerTimeout)
}

// value: none、gzip、snappy、lz4、zstd
func WithProducerCompression(ProducerCompression string) broker.PublishOption {
	return broker.PublishContextWithValue(producerCompression{}, ProducerCompression)
}

func WithProducerCompressionLevel(ProducerCompressionLevel int) broker.PublishOption {
	return broker.PublishContextWithValue(producerCompressionLevel{}, ProducerCompressionLevel)
}

// value: hash、murmur2_hash、random、random、round_robin、reference、manual
func WithProducerPartitioner(ProducerPartitioner string) broker.PublishOption {
	return broker.PublishContextWithValue(producerPartitioner{}, ProducerPartitioner)
}

func WithProducerIdempotent(ProducerIdempotent bool) broker.PublishOption {
	return broker.PublishContextWithValue(producerIdempotent{}, ProducerIdempotent)
}

func WithProducerTransactionID(ProducerTransactionID string) broker.PublishOption {
	return broker.PublishContextWithValue(producerTransactionIDKey{}, ProducerTransactionID)
}

func WithProducerTransactionTimeout(ProducerTransactionTimeout time.Duration) broker.PublishOption {
	return broker.PublishContextWithValue(producerTransactionTimeoutKey{}, ProducerTransactionTimeout)
}

func WithProducerTransactionRetryMax(ProducerTransactionRetryMax int) broker.PublishOption {
	return broker.PublishContextWithValue(producerTransactionRetryMaxKey{}, ProducerTransactionRetryMax)
}

func WithProducerTransactionRetryBackoff(ProducerTransactionRetryBackoff time.Duration) broker.PublishOption {
	return broker.PublishContextWithValue(producerTransactionRetryBackoffKey{}, ProducerTransactionRetryBackoff)
}

func WithProducerTransactionRetryBackoffFunc(ProducerTransactionRetryBackoffFunc func(retries, maxRetries int) time.Duration) broker.PublishOption {
	return broker.PublishContextWithValue(producerTransactionRetryBackoffFuncKey{}, ProducerTransactionRetryBackoffFunc)
}

func WithProducerReturnSuccesses(ProducerReturnSuccesses bool) broker.PublishOption {
	return broker.PublishContextWithValue(producerReturnSuccesses{}, ProducerReturnSuccesses)
}

func WithProducerReturnErrors(ProducerReturnErrors bool) broker.PublishOption {
	return broker.PublishContextWithValue(producerReturnErrors{}, ProducerReturnErrors)
}

func WithProducerFlushBytes(ProducerFlushBytes int) broker.PublishOption {
	return broker.PublishContextWithValue(producerFlushBytes{}, ProducerFlushBytes)
}

func WithProducerFlushMessages(ProducerFlushMessages int) broker.PublishOption {
	return broker.PublishContextWithValue(producerFlushMessages{}, ProducerFlushMessages)
}

func WithProducerFlushFrequency(ProducerFlushFrequency time.Duration) broker.PublishOption {
	return broker.PublishContextWithValue(producerFlushFrequency{}, ProducerFlushFrequency)
}

func WithProducerFlushMaxMessages(ProducerFlushMaxMessages int) broker.PublishOption {
	return broker.PublishContextWithValue(producerFlushMaxMessages{}, ProducerFlushMaxMessages)
}

func WithProducerRetryMax(ProducerRetryMax int) broker.PublishOption {
	return broker.PublishContextWithValue(producerRetryMax{}, ProducerRetryMax)
}

func WithProducerRetryBackoff(ProducerRetryBackoff time.Duration) broker.PublishOption {
	return broker.PublishContextWithValue(producerRetryBackoff{}, ProducerRetryBackoff)
}

func WithProducerRetryBackoffFunc(ProducerRetryBackoffFunc func(retries, maxRetries int) time.Duration) broker.PublishOption {
	return broker.PublishContextWithValue(producerRetryBackoffFunc{}, ProducerRetryBackoffFunc)
}

func WithProducerInterceptors(ProducerInterceptors []sarama.ProducerInterceptor) broker.PublishOption {
	return broker.PublishContextWithValue(producerInterceptors{}, ProducerInterceptors)
}

func WithConsumerGroupSessionTimeout(ConsumerGroupSessionTimeout time.Duration) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerGroupSessionTimeout{}, ConsumerGroupSessionTimeout)
}

func WithConsumerGroupHeartbeatInterval(ConsumerGroupHeartbeatInterval time.Duration) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerGroupHeartbeatInterval{}, ConsumerGroupHeartbeatInterval)
}

// value: range、roundrobin、sticky
// Deprecated: Strategy exists for historical compatibility
// and should not be used. Please use GroupStrategies.
func WithConsumerGroupRebalanceStrategy(ConsumerGroupRebalanceStrategy string) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerGroupRebalanceStrategy{}, ConsumerGroupRebalanceStrategy)
}

// value: range、roundrobin、sticky
func WithConsumerGroupRebalanceGroupStrategies(ConsumerGroupRebalanceGroupStrategies []string) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerGroupRebalanceGroupStrategies{}, ConsumerGroupRebalanceGroupStrategies)
}
func WithConsumerGroupRebalanceTimeout(ConsumerGroupRebalanceTimeout time.Duration) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerGroupRebalanceTimeout{}, ConsumerGroupRebalanceTimeout)
}

func WithConsumerGroupRebalanceRetryMax(ConsumerGroupRebalanceRetryMax int) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerGroupRebalanceRetryMax{}, ConsumerGroupRebalanceRetryMax)
}

func WithConsumerGroupRebalanceRetryBackoff(ConsumerGroupRebalanceRetryBackoff time.Duration) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerGroupRebalanceRetryBackoff{}, ConsumerGroupRebalanceRetryBackoff)
}

func WithConsumerGroupMemberUserData(ConsumerGroupMemberUserData []byte) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerGroupMemberUserData{}, ConsumerGroupMemberUserData)
}

func WithConsumerGroupInstanceId(ConsumerGroupInstanceId string) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerGroupInstanceIdKey{}, ConsumerGroupInstanceId)
}

func WithConsumerGroupResetInvalidOffsets(ConsumerGroupResetInvalidOffsets bool) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerGroupResetInvalidOffsetsKey{}, ConsumerGroupResetInvalidOffsets)
}

func WithConsumerRetryBackoff(ConsumerRetryBackoff time.Duration) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerRetryBackoff{}, ConsumerRetryBackoff)
}

func WithConsumerRetryBackoffFunc(ConsumerRetryBackoffFunc func(retries int) time.Duration) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerRetryBackoffFunc{}, ConsumerRetryBackoffFunc)
}

func WithConsumerFetchMin(ConsumerFetchMin int32) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerFetchMin{}, ConsumerFetchMin)
}

func WithConsumerFetchDefault(ConsumerFetchDefault int32) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerFetchDefault{}, ConsumerFetchDefault)
}

func WithConsumerFetchMax(ConsumerFetchMax int32) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerFetchMax{}, ConsumerFetchMax)
}

func WithConsumerMaxWaitTime(ConsumerMaxWaitTime time.Duration) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerMaxWaitTime{}, ConsumerMaxWaitTime)
}

func WithConsumerMaxProcessingTime(ConsumerMaxProcessingTime time.Duration) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerMaxProcessingTime{}, ConsumerMaxProcessingTime)
}

func WithConsumerReturnErrors(ConsumerReturnErrors bool) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerReturnErrors{}, ConsumerReturnErrors)
}

func WithConsumerOffsetsAutoCommitEnable(ConsumerOffsetsAutoCommitEnable bool) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerOffsetsAutoCommitEnable{}, ConsumerOffsetsAutoCommitEnable)
}

func WithConsumerOffsetsAutoCommitInterval(ConsumerOffsetsAutoCommitInterval time.Duration) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerOffsetsAutoCommitInterval{}, ConsumerOffsetsAutoCommitInterval)
}

func WithConsumerOffsetsInitial(ConsumerOffsetsInitial int64) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerOffsetsInitial{}, ConsumerOffsetsInitial)
}

func WithConsumerOffsetsRetention(ConsumerOffsetsRetention time.Duration) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerOffsetsRetention{}, ConsumerOffsetsRetention)
}

func WithConsumerOffsetsRetryMax(ConsumerOffsetsRetryMax int) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerOffsetsRetryMax{}, ConsumerOffsetsRetryMax)
}

// value: （0：ReadUncommitted）、（1：ReadCommitted）
func WithConsumerIsolationLevel(ConsumerIsolationLevel int8) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerIsolationLevel{}, ConsumerIsolationLevel)
}

func WithConsumerInterceptors(ConsumerInterceptors []sarama.ConsumerInterceptor) broker.SubscribeOption {
	return broker.SubscribeContextWithValue(consumerInterceptors{}, ConsumerInterceptors)
}

func WithClientID(ClientID string) broker.Option {
	return broker.OptionContextWithValue(clientID{}, ClientID)
}

func WithRackID(RackID string) broker.Option {
	return broker.OptionContextWithValue(rackID{}, RackID)
}

func WithChannelBufferSize(ChannelBufferSize int) broker.Option {
	return broker.OptionContextWithValue(channelBufferSize{}, ChannelBufferSize)
}

func WithApiVersionsRequest(ApiVersionsRequest bool) broker.Option {
	return broker.OptionContextWithValue(apiVersionsRequest{}, ApiVersionsRequest)
}

func WithVersion(Version string) broker.Option {
	return broker.OptionContextWithValue(version{}, Version)
}
