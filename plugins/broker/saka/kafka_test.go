package saka

import (
	"context"
	"fmt"
	"gitee.com/weixuehao/kratos-bootloader/plugins/broker"
	"github.com/stretchr/testify/assert"
	"log"
	"math/rand"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"
	"testing"
	"time"
)

var addr = "192.168.6.56:9092,192.168.6.56:9093,192.168.6.56:9094"
var topic = "hello"

// 测试同步发送
func Test_Publish_WithRawData(t *testing.T) {
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	split := strings.Split(addr, ",")

	b := NewSaramaKafka()

	if err := b.Init(
		broker.WithAddress(split...),
	); err != nil {
		t.Logf("cant Init to broker, skip: %v", err)
		t.Skip()
	}

	if err := b.Connect(); err != nil {
		t.Logf("cant connect to broker, skip: %v", err)
		t.Skip()
	}

	var msg broker.Message
	const count = 10
	for i := 0; i < 100; i++ {
		startTime := time.Now()
		msg.Value = []byte(strconv.Itoa(rand.Intn(100)))
		//默认同步发送
		pub, err := b.Publish(topic, &msg)
		fmt.Printf("par: %v,offset: %v \n", pub.Message().Partition, pub.Message().Offset)
		assert.Nil(t, err)
		elapsedTime := time.Since(startTime) / time.Millisecond
		fmt.Printf("Publish %d, elapsed time: %dms, value: %v \n",
			i, elapsedTime, string(msg.Value))
		time.Sleep(time.Millisecond * 500)
	}

	fmt.Printf("total send %d messages\n", count)

	<-interrupt
	err := b.Disconnect()
	if err != nil {
		log.Println(err)
	}
}

// 测试单协程消费、手动提交ACK（支持自动ACK）
func Test_Subscribe_WithRawData(t *testing.T) {
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	split := strings.Split(addr, ",")
	b := NewSaramaKafka()

	if err := b.Init(
		broker.WithAddress(split...),
	); err != nil {
		t.Logf("cant Init to broker, skip: %v", err)
		t.Skip()
	}

	if err := b.Connect(); err != nil {
		t.Logf("cant connect to broker, skip: %v", err)
		t.Skip()
	}

	h := func(ctx context.Context, event broker.Event) error {
		value := event.Message().Value
		claims := event.Claims()
		log.Printf("consumer get claims: %v,par: %v, offset: %v, msg value: %v", claims, event.Message().Partition, event.Message().Offset, string(value))
		err := event.Ack("")
		if err != nil {
			log.Printf(err.Error())
		}

		return nil
	}
	_, err := b.Subscribe([]string{topic},
		h,
		broker.WithQueueName("hello-group-1"),
		broker.WithDisableAutoAck(),
	)
	assert.Nil(t, err)
	assert.Nil(t, err)

	<-interrupt
	err = b.Disconnect()
	if err != nil {
		log.Println(err)
	}
}

// 测试增加分区、自定义版本
func Test_AddPartition(t *testing.T) {

	split := strings.Split(addr, ",")
	b := NewSaramaKafka()
	err := b.Init(broker.WithAddress(split...), WithVersion("0.11.0.2"))
	if err != nil {
		t.Logf("cant Init to broker, skip: %v", err)
		t.Skip()
	}

	if err := b.Connect(); err != nil {
		t.Logf("cant connect to broker, skip: %v", err)
		t.Skip()
	}
	manager, err := b.ClientManager()
	if err != nil {
		t.Logf("cant get manager, skip: %v", err)
		t.Skip()
	}
	err = manager.CreatePartition(topic, 4)
	if err != nil {
		t.Logf("cant create partition, skip: %v", err)
		t.Skip()
	}

	err = b.Disconnect()
	if err != nil {
		log.Println(err)
	}

}

// 测试获取分区
func Test_GetPartition(t *testing.T) {

	split := strings.Split(addr, ",")
	b := NewSaramaKafka()
	err := b.Init(broker.WithAddress(split...), WithVersion("1.0.0"))
	if err != nil {
		t.Logf("cant Init to broker, skip: %v", err)
		t.Skip()
	}

	if err := b.Connect(); err != nil {
		t.Logf("cant connect to broker, skip: %v", err)
		t.Skip()
	}
	manager, err := b.ClientManager()
	if err != nil {
		t.Logf("cant get manager, skip: %v", err)
		t.Skip()
	}
	par, err := manager.GetPartitions(topic)
	if err != nil {
		t.Logf("cant create partition, skip: %v", err)
		t.Skip()
	}
	fmt.Println(par)

	err = b.Disconnect()
	if err != nil {
		log.Println(err)
	}

}

// 测试header、异步发送、成功回调
func Test_Publish_WithHeader(t *testing.T) {
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)

	split := strings.Split(addr, ",")

	b := NewSaramaKafka()

	if err := b.Init(
		broker.WithAddress(split...),
	); err != nil {
		t.Logf("cant Init to broker, skip: %v", err)
		t.Skip()
	}

	if err := b.Connect(); err != nil {
		t.Logf("cant connect to broker, skip: %v", err)
		t.Skip()
	}

	h := func(publisher2 broker.Publisher) {
		log.Printf("发布成功回调，header: %v, par: %v, offset: %v, key: %v, value: %v \n", publisher2.Message().Headers, publisher2.Message().Partition, publisher2.Message().Offset, publisher2.Message().Key, string(publisher2.Message().Value))

	}

	//var headers map[string][]byte
	//headers = make(map[string][]byte)
	//headers["version"] = []byte("1.0.0")
	var msg broker.Message
	const count = 100
	for i := 0; i < count; i++ {
		startTime := time.Now()
		//headers["trace_id"] = []byte(strconv.Itoa(i))
		//msg.Key = strconv.Itoa(i)
		msg.Value = []byte(strconv.Itoa(rand.Intn(100)))
		//msg.Headers = headers
		_, err := b.Publish(topic, &msg, broker.WithEnableAsync(true), WithProducerAsyncProduceSuccessKey(h))
		assert.Nil(t, err)
		elapsedTime := time.Since(startTime) / time.Millisecond
		fmt.Printf("Publish %d, elapsed time: %dms, value: %v \n",
			i, elapsedTime, string(msg.Value))
	}

	fmt.Printf("total send %d messages\n", count)

	<-interrupt

	err := b.Disconnect()
	if err != nil {
		log.Println(err)
	}
}

// 测试多协程并发消费
func Test_Subscribe_WithGoroutine(t *testing.T) {
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	split := strings.Split(addr, ",")
	b := NewSaramaKafka()

	if err := b.Init(
		broker.WithAddress(split...),
	); err != nil {
		t.Logf("cant Init to broker, skip: %v", err)
		t.Skip()
	}

	if err := b.Connect(); err != nil {
		t.Logf("cant connect to broker, skip: %v", err)
		t.Skip()
	}

	ch := make(chan broker.Event, 1000)

	_, err := b.Subscribe([]string{topic},
		nil,
		broker.WithQueueName("hello-group-1"),
		broker.WithMsgHandleChan(ch),
	)
	assert.Nil(t, err)
	assert.Nil(t, err)

	go func() {
		for {
			select {
			case event := <-ch:
				value := event.Message().Value
				claims := event.Claims()
				log.Printf("consumer get claims: %v,par: %v, offset: %v, header: %v, key: %v, msg value: %v \n", claims, event.Message().Partition, event.Message().Offset, event.Message().Headers, event.Message().Key, string(value))
				err := event.Ack("")
				if err != nil {
					log.Printf(err.Error())
				}
			}
		}
	}()

	<-interrupt

	err = b.Disconnect()
	if err != nil {
		log.Println(err)
	}
}
