package saka

import (
	"gitee.com/weixuehao/kratos-bootloader/plugins/broker"
	"github.com/IBM/sarama"
	"github.com/go-kratos/kratos/v2/log"
)

type publisher struct {
	sp                      sarama.SyncProducer
	sps                     map[string]sarama.SyncProducer
	ap                      sarama.AsyncProducer
	aps                     map[string]sarama.AsyncProducer
	enableOneTopicOneWriter bool
	async                   bool
}

func newPublisher() *publisher {
	return &publisher{
		sps: make(map[string]sarama.SyncProducer),
		aps: make(map[string]sarama.AsyncProducer),
	}
}

func (w *publisher) close() {
	if w.sp != nil {
		_ = w.sp.Close()
	}
	if w.ap != nil {
		_ = w.ap.Close()
	}
	for _, writer := range w.sps {
		_ = writer.Close()
	}
	for _, writer := range w.aps {
		_ = writer.Close()
	}
	w.sp = nil
	w.sps = nil
	w.ap = nil
	w.aps = nil
}

// createSyncProducer create sarama SyncProducer
func (w *publisher) createSyncProducer(addr []string, logger *log.Helper, conf *sarama.Config, pubops broker.PublishOptions) (sarama.SyncProducer, error) {
	config := getClusterConfig(conf, pubops, logger)
	config.Producer.Return.Successes = true
	config.Producer.Return.Errors = true

	err2 := config.Validate()
	if err2 != nil {
		return nil, err2
	}

	sp, err := sarama.NewSyncProducer(addr, config)

	if err != nil {
		return nil, err
	}

	return sp, nil
}

// createAsyncProducer create sarama AsyncProducer
func (w *publisher) createAsyncProducer(addr []string, logger *log.Helper, conf *sarama.Config, pubops broker.PublishOptions) (sarama.AsyncProducer, error) {

	var (
		errors    func(broker.Publisher)
		successes func(broker.Publisher)
	)
	if c, ok := pubops.CtxValue.Value(asyncProduceErrorKey{}).(func(broker.Publisher)); ok {
		errors = c
	}
	if c, ok := pubops.CtxValue.Value(asyncProduceSuccessKey{}).(func(broker.Publisher)); ok {
		successes = c
	}
	config := getClusterConfig(conf, pubops, logger)
	if errors != nil {
		config.Producer.Return.Errors = true
	}

	if successes != nil {
		config.Producer.Return.Successes = true
	}

	err2 := config.Validate()
	if err2 != nil {
		return nil, err2
	}

	ap, err := sarama.NewAsyncProducer(addr, config)
	if err != nil {
		return nil, err
	}
	if errors != nil {
		go func() {
			for {
				v, ok := <-ap.Errors()
				// channel closed, the producer is stopping
				if !ok {
					return
				}
				pub := &publication{msg: &broker.Message{Headers: make(map[string][]byte)}}
				pub.err = v.Err
				if v.Msg.Key != nil && v.Msg.Key.Length() > 0 {
					encode, _ := v.Msg.Key.Encode()
					pub.msg.Key = string(encode)
				}

				if v.Msg.Value != nil && v.Msg.Value.Length() > 0 {
					pub.msg.Value, _ = v.Msg.Value.Encode()
				}
				pub.msg.Topic = v.Msg.Topic
				pub.msg.Timestamp = v.Msg.Timestamp
				for _, header := range v.Msg.Headers {
					pub.msg.Headers[string(header.Key)] = header.Value
				}
				pub.msg.Offset = v.Msg.Offset
				pub.msg.Partition = v.Msg.Partition
				errors(pub)
			}
		}()
	}

	if successes != nil {
		go func() {
			for {
				v, ok := <-ap.Successes()
				// channel closed, the producer is stopping
				if !ok {
					return
				}
				pub := &publication{msg: &broker.Message{Headers: make(map[string][]byte)}}
				if v.Key != nil && v.Key.Length() > 0 {
					encode, _ := v.Key.Encode()
					pub.msg.Key = string(encode)
				}

				if v.Value != nil && v.Value.Length() > 0 {
					pub.msg.Value, _ = v.Value.Encode()
				}
				pub.msg.Topic = v.Topic
				pub.msg.Timestamp = v.Timestamp
				for _, header := range v.Headers {
					pub.msg.Headers[string(header.Key)] = header.Value
				}
				pub.msg.Offset = v.Offset
				pub.msg.Partition = v.Partition
				successes(pub)
			}
		}()
	}

	if config.Producer.Return.Errors == true && errors == nil {
		go func() {
			for {
				v, ok := <-ap.Errors()
				// channel closed, the producer is stopping
				if !ok {
					return
				}
				logger.Errorf("topic=%v, partitions=%d, offset=%d, value=%v, error= %v", v.Msg.Topic, v.Msg.Partition, v.Msg.Offset, v.Msg.Value, v.Err)
			}
		}()
	}

	if config.Producer.Return.Successes == true && successes == nil {
		go func() {
			for {
				v, ok := <-ap.Successes()
				// channel closed, the producer is stopping
				if !ok {
					return
				}
				logger.Infof("topic=%v, partitions=%d, offset=%d, value=%v", v.Topic, v.Partition, v.Offset, v.Value)
			}
		}()
	}

	return ap, nil

}

type publication struct {
	topic         string
	msg           *broker.Message
	syncProducer  sarama.SyncProducer
	asyncProducer sarama.AsyncProducer
	err           error
}

func (p *publication) Topic() string {
	return p.topic
}

func (p *publication) Message() *broker.Message {
	return p.msg
}
func (p *publication) Error() error {
	return p.err
}

func (p *publication) Close() error {
	if p.syncProducer != nil {
		err := p.syncProducer.Close()
		if err != nil {
			return err
		}
	}
	if p.asyncProducer != nil {
		err := p.asyncProducer.Close()
		if err != nil {
			return err
		}
	}
	return nil
}
