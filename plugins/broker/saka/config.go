package saka

import (
	"crypto/tls"
	"fmt"
	"gitee.com/weixuehao/kratos-bootloader/plugins/broker"
	"github.com/IBM/sarama"
	"github.com/go-kratos/kratos/v2/log"
	"golang.org/x/net/proxy"
	"hash"
	"time"
)

func getClusterConfig(conf *sarama.Config, opt broker.Opt, logger *log.Helper) *sarama.Config {

	if value, ok := opt.Ctx().Value(clusterConfigKey{}).(*sarama.Config); ok {
		return value
	}

	if value, ok := opt.Ctx().Value(adminRetryMax{}).(int); ok {
		conf.Admin.Retry.Max = value
	}
	if value, ok := opt.Ctx().Value(adminRetryBackoff{}).(time.Duration); ok {
		conf.Admin.Retry.Backoff = value
	}
	if value, ok := opt.Ctx().Value(adminTimeout{}).(time.Duration); ok {
		conf.Admin.Timeout = value
	}
	if value, ok := opt.Ctx().Value(netMaxOpenRequests{}).(int); ok {
		conf.Net.MaxOpenRequests = value
	}
	if value, ok := opt.Ctx().Value(netDialTimeout{}).(time.Duration); ok {
		conf.Net.DialTimeout = value
	}
	if value, ok := opt.Ctx().Value(netReadTimeout{}).(time.Duration); ok {
		conf.Net.ReadTimeout = value
	}
	if value, ok := opt.Ctx().Value(netWriteTimeout{}).(time.Duration); ok {
		conf.Net.WriteTimeout = value
	}
	if value, ok := opt.Ctx().Value(netTLSEnable{}).(bool); ok {
		conf.Net.TLS.Enable = value
	}
	if value, ok := opt.Ctx().Value(netTLSConfig{}).(*tls.Config); ok {
		conf.Net.TLS.Config = value
	}
	if value, ok := opt.Ctx().Value(netSASLEnable{}).(bool); ok {
		conf.Net.SASL.Enable = value
	}
	if value, ok := opt.Ctx().Value(netSASLMechanismKey{}).(string); ok {
		conf.Net.SASL.Mechanism = sarama.SASLMechanism(value)
	}

	if value, ok := opt.Ctx().Value(netSASLVersion{}).(int16); ok {
		conf.Net.SASL.Version = value
	}

	if value, ok := opt.Ctx().Value(netSASLHandshakeKey{}).(bool); ok {
		conf.Net.SASL.Handshake = value
	}
	if value, ok := opt.Ctx().Value(netSASLAuthIdentityKey{}).(string); ok {
		conf.Net.SASL.AuthIdentity = value
	}
	if value, ok := opt.Ctx().Value(netSASLUser{}).(string); ok {
		conf.Net.SASL.User = value
	}
	if value, ok := opt.Ctx().Value(netSASLPassword{}).(string); ok {
		conf.Net.SASL.Password = value
	}

	if value, ok := opt.Ctx().Value(netSASLSCRAMAuthzIDKey{}).(string); ok {
		conf.Net.SASL.SCRAMAuthzID = value
	}

	if value, ok := opt.Ctx().Value(netSASLSCRAMClientGeneratorFuncKey{}).(func() sarama.SCRAMClient); ok {
		conf.Net.SASL.SCRAMClientGeneratorFunc = value
	}
	if value, ok := opt.Ctx().Value(netSASLTokenProviderKey{}).(sarama.AccessTokenProvider); ok {
		conf.Net.SASL.TokenProvider = value
	}

	if value, ok := opt.Ctx().Value(netKeepAlive{}).(time.Duration); ok {
		conf.Net.KeepAlive = value
	}
	if value, ok := opt.Ctx().Value(netProxyEnable{}).(bool); ok {
		conf.Net.Proxy.Enable = value
	}
	if value, ok := opt.Ctx().Value(netProxyDialer{}).(proxy.Dialer); ok {
		conf.Net.Proxy.Dialer = value
	}
	if value, ok := opt.Ctx().Value(metadataRetryMax{}).(int); ok {
		conf.Metadata.Retry.Max = value
	}
	if value, ok := opt.Ctx().Value(metadataRetryBackoff{}).(time.Duration); ok {
		conf.Metadata.Retry.Backoff = value
	}
	if value, ok := opt.Ctx().Value(metadataRetryBackoffFunc{}).(func(retries, maxRetries int) time.Duration); ok {
		conf.Metadata.Retry.BackoffFunc = value
	}
	if value, ok := opt.Ctx().Value(metadataRefreshFrequency{}).(time.Duration); ok {
		conf.Metadata.RefreshFrequency = value
	}
	if value, ok := opt.Ctx().Value(metadataFull{}).(bool); ok {
		conf.Metadata.Full = value
	}
	if value, ok := opt.Ctx().Value(metadataTimeout{}).(time.Duration); ok {
		conf.Metadata.Timeout = value
	}
	if value, ok := opt.Ctx().Value(metadataAllowAutoTopicCreation{}).(bool); ok {
		conf.Metadata.AllowAutoTopicCreation = value
	}
	if value, ok := opt.Ctx().Value(producerMaxMessageBytes{}).(int); ok {
		conf.Producer.MaxMessageBytes = value
	}
	if value, ok := opt.Ctx().Value(producerRequiredAcks{}).(int16); ok {
		switch value {
		case 0:
			conf.Producer.RequiredAcks = sarama.NoResponse
			break
		case 1:
			conf.Producer.RequiredAcks = sarama.WaitForLocal
			break
		case -1:
			conf.Producer.RequiredAcks = sarama.WaitForAll
			break
		default:
			logger.Errorf("Producer RequiredAcks not recognised : %v", value)
			conf.Producer.RequiredAcks = sarama.WaitForLocal
		}
	}
	if value, ok := opt.Ctx().Value(producerTimeout{}).(time.Duration); ok {
		conf.Producer.Timeout = value
	}
	if value, ok := opt.Ctx().Value(producerCompression{}).(string); ok {
		compression, err := strToCompressionCodec(value)
		if err != nil {
			logger.Error(err)
		}
		conf.Producer.Compression = compression
	}
	if value, ok := opt.Ctx().Value(producerCompressionLevel{}).(int); ok {
		conf.Producer.CompressionLevel = value
	}
	if value, ok := opt.Ctx().Value(producerPartitioner{}).(string); ok {
		partitioner, err := strToPartitioner(value)
		if err != nil {
			logger.Error(err)
		} else {
			conf.Producer.Partitioner = partitioner
		}
	}

	if value, ok := opt.Ctx().Value(producerIdempotent{}).(bool); ok {
		conf.Producer.Idempotent = value
	}

	if value, ok := opt.Ctx().Value(producerTransactionIDKey{}).(string); ok {
		conf.Producer.Transaction.ID = value
	}
	if value, ok := opt.Ctx().Value(producerTransactionTimeoutKey{}).(time.Duration); ok {
		conf.Producer.Transaction.Timeout = value
	}
	if value, ok := opt.Ctx().Value(producerTransactionRetryMaxKey{}).(int); ok {
		conf.Producer.Transaction.Retry.Max = value
	}
	if value, ok := opt.Ctx().Value(producerTransactionRetryBackoffKey{}).(time.Duration); ok {
		conf.Producer.Transaction.Retry.Backoff = value
	}
	if value, ok := opt.Ctx().Value(producerTransactionRetryBackoffFuncKey{}).(func(retries, maxRetries int) time.Duration); ok {
		conf.Producer.Transaction.Retry.BackoffFunc = value
	}

	if value, ok := opt.Ctx().Value(producerReturnSuccesses{}).(bool); ok {
		conf.Producer.Return.Successes = value
	} else {
		conf.Producer.Return.Successes = false
	}
	if value, ok := opt.Ctx().Value(producerReturnErrors{}).(bool); ok {
		conf.Producer.Return.Errors = value
	} else {
		conf.Producer.Return.Errors = true
	}
	if value, ok := opt.Ctx().Value(producerFlushBytes{}).(int); ok {
		conf.Producer.Flush.Bytes = value
	}
	if value, ok := opt.Ctx().Value(producerFlushMessages{}).(int); ok {
		conf.Producer.Flush.Messages = value
	}
	if value, ok := opt.Ctx().Value(producerFlushFrequency{}).(time.Duration); ok {
		conf.Producer.Flush.Frequency = value
	}
	if value, ok := opt.Ctx().Value(producerFlushMaxMessages{}).(int); ok {
		conf.Producer.Flush.MaxMessages = value
	}
	if value, ok := opt.Ctx().Value(producerRetryMax{}).(int); ok {
		conf.Producer.Retry.Max = value
	}
	if value, ok := opt.Ctx().Value(producerRetryBackoff{}).(time.Duration); ok {
		conf.Producer.Retry.Backoff = value
	}
	if value, ok := opt.Ctx().Value(producerRetryBackoffFunc{}).(func(retries, maxRetries int) time.Duration); ok {
		conf.Producer.Retry.BackoffFunc = value
	}
	if value, ok := opt.Ctx().Value(producerInterceptors{}).([]sarama.ProducerInterceptor); ok {
		conf.Producer.Interceptors = value
	}
	if value, ok := opt.Ctx().Value(consumerGroupSessionTimeout{}).(time.Duration); ok {
		conf.Consumer.Group.Session.Timeout = value
	}
	if value, ok := opt.Ctx().Value(consumerGroupHeartbeatInterval{}).(time.Duration); ok {
		conf.Consumer.Group.Heartbeat.Interval = value
	}
	if value, ok := opt.Ctx().Value(consumerGroupRebalanceStrategy{}).(string); ok {
		switch value {
		case "range":
			conf.Consumer.Group.Rebalance.Strategy = sarama.BalanceStrategyRange
			break
		case "roundrobin":
			conf.Consumer.Group.Rebalance.Strategy = sarama.BalanceStrategyRoundRobin
			break
		case "sticky":
			conf.Consumer.Group.Rebalance.Strategy = sarama.BalanceStrategySticky
			break
		default:
			conf.Consumer.Group.Rebalance.Strategy = sarama.BalanceStrategyRange
			logger.Errorf("Consumer Group RebalanceStrategy not recognised : %v", value)
		}
	}
	if value, ok := opt.Ctx().Value(consumerGroupRebalanceGroupStrategies{}).([]string); ok {
		strategy, err := strToBalanceStrategy(value)
		if err != nil {
			logger.Error(err)
		} else {
			conf.Consumer.Group.Rebalance.GroupStrategies = strategy
		}

	}
	if value, ok := opt.Ctx().Value(consumerGroupRebalanceTimeout{}).(time.Duration); ok {
		conf.Consumer.Group.Rebalance.Timeout = value
	}
	if value, ok := opt.Ctx().Value(consumerGroupRebalanceRetryMax{}).(int); ok {
		conf.Consumer.Group.Rebalance.Retry.Max = value
	}
	if value, ok := opt.Ctx().Value(consumerGroupRebalanceRetryBackoff{}).(time.Duration); ok {
		conf.Consumer.Group.Rebalance.Retry.Backoff = value
	}
	if value, ok := opt.Ctx().Value(consumerGroupMemberUserData{}).([]byte); ok {
		conf.Consumer.Group.Member.UserData = value
	}

	if value, ok := opt.Ctx().Value(consumerGroupInstanceIdKey{}).(string); ok {
		conf.Consumer.Group.InstanceId = value
	}
	if value, ok := opt.Ctx().Value(consumerGroupResetInvalidOffsetsKey{}).(bool); ok {
		conf.Consumer.Group.ResetInvalidOffsets = value
	}
	if value, ok := opt.Ctx().Value(consumerRetryBackoff{}).(time.Duration); ok {
		conf.Consumer.Retry.Backoff = value
	}
	if value, ok := opt.Ctx().Value(consumerRetryBackoffFunc{}).(func(retries int) time.Duration); ok {
		conf.Consumer.Retry.BackoffFunc = value
	}
	if value, ok := opt.Ctx().Value(consumerFetchMin{}).(int32); ok {
		conf.Consumer.Fetch.Min = value
	}
	if value, ok := opt.Ctx().Value(consumerFetchDefault{}).(int32); ok {
		conf.Consumer.Fetch.Default = value
	}
	if value, ok := opt.Ctx().Value(consumerFetchMax{}).(int32); ok {
		conf.Consumer.Fetch.Max = value
	}
	if value, ok := opt.Ctx().Value(consumerMaxWaitTime{}).(time.Duration); ok {
		conf.Consumer.MaxWaitTime = value
	}
	if value, ok := opt.Ctx().Value(consumerMaxProcessingTime{}).(time.Duration); ok {
		conf.Consumer.MaxProcessingTime = value
	}
	if value, ok := opt.Ctx().Value(consumerReturnErrors{}).(bool); ok {
		conf.Consumer.Return.Errors = value
	} else {
		conf.Consumer.Return.Errors = true
	}
	if value, ok := opt.Ctx().Value(consumerOffsetsAutoCommitEnable{}).(bool); ok {
		conf.Consumer.Offsets.AutoCommit.Enable = value
	}
	if value, ok := opt.Ctx().Value(consumerOffsetsAutoCommitInterval{}).(time.Duration); ok {
		conf.Consumer.Offsets.AutoCommit.Interval = value
	}
	if value, ok := opt.Ctx().Value(consumerOffsetsInitial{}).(int64); ok {
		conf.Consumer.Offsets.Initial = value
	}
	if value, ok := opt.Ctx().Value(consumerOffsetsRetention{}).(time.Duration); ok {
		conf.Consumer.Offsets.Retention = value
	}
	if value, ok := opt.Ctx().Value(consumerOffsetsRetryMax{}).(int); ok {
		conf.Consumer.Offsets.Retry.Max = value
	}
	if value, ok := opt.Ctx().Value(consumerIsolationLevel{}).(int8); ok {
		switch value {
		case 0:
			conf.Consumer.IsolationLevel = sarama.ReadUncommitted
		case 1:
			conf.Consumer.IsolationLevel = sarama.ReadCommitted
		default:
			logger.Errorf("Consumer IsolationLevel not recognised : %v", value)
		}
	}
	if value, ok := opt.Ctx().Value(consumerInterceptors{}).([]sarama.ConsumerInterceptor); ok {
		conf.Consumer.Interceptors = value
	}
	if value, ok := opt.Ctx().Value(clientID{}).(string); ok {
		conf.ClientID = value
	}
	if value, ok := opt.Ctx().Value(rackID{}).(string); ok {
		conf.RackID = value
	}
	if value, ok := opt.Ctx().Value(channelBufferSize{}).(int); ok {
		conf.ChannelBufferSize = value
	}
	if value, ok := opt.Ctx().Value(apiVersionsRequest{}).(bool); ok {
		conf.ApiVersionsRequest = value
	}
	if value, ok := opt.Ctx().Value(version{}).(string); ok {
		version, err := sarama.ParseKafkaVersion(value)
		if err != nil {
			logger.Errorf("Error parsing Kafka version: %v", err)
		}
		conf.Version = version
	} else {
		conf.Version = sarama.V2_0_0_0
	}
	return conf
}

func strToCompressionCodec(str string) (sarama.CompressionCodec, error) {
	switch str {
	case "none":
		return sarama.CompressionNone, nil
	case "snappy":
		return sarama.CompressionSnappy, nil
	case "lz4":
		return sarama.CompressionLZ4, nil
	case "gzip":
		return sarama.CompressionGZIP, nil
	case "zstd":
		return sarama.CompressionZSTD, nil
	}
	return sarama.CompressionNone, fmt.Errorf("compression codec not recognised: %v", str)
}

func strToPartitioner(str string) (sarama.PartitionerConstructor, error) {
	switch str {
	case "hash":
		return sarama.NewHashPartitioner, nil
	case "murmur2_hash":
		return sarama.NewCustomPartitioner(
			sarama.WithAbsFirst(),
			sarama.WithCustomHashFunction(newMurmur2Hash32),
		), nil
	case "random":
		return sarama.NewRandomPartitioner, nil
	case "round_robin":
		return sarama.NewRoundRobinPartitioner, nil
	case "reference":
		return sarama.NewReferenceHashPartitioner, nil
	case "manual":
		return sarama.NewManualPartitioner, nil
	default:
	}
	return nil, fmt.Errorf("partitioner not recognised: %v", str)
}

type murmur2 struct {
	data   []byte
	cached *uint32
}

func newMurmur2Hash32() hash.Hash32 {
	return &murmur2{
		data: make([]byte, 0),
	}
}

// Write a slice of data to the hasher.
func (mur *murmur2) Write(p []byte) (n int, err error) {
	mur.data = append(mur.data, p...)
	mur.cached = nil
	return len(p), nil
}

// Sum appends the current hash to b and returns the resulting slice.
// It does not change the underlying hash state.
func (mur *murmur2) Sum(b []byte) []byte {
	v := mur.Sum32()
	return append(b, byte(v>>24), byte(v>>16), byte(v>>8), byte(v))
}

// Reset resets the Hash to its initial state.
func (mur *murmur2) Reset() {
	mur.data = mur.data[0:0]
	mur.cached = nil
}

// Size returns the number of bytes Sum will return.
func (mur *murmur2) Size() int {
	return 4
}

// BlockSize returns the hash's underlying block size.
// The Write method must be able to accept any amount
// of data, but it may operate more efficiently if all writes
// are a multiple of the block size.
func (mur *murmur2) BlockSize() int {
	return 4
}

const (
	seed uint32 = uint32(0x9747b28c)
	m    int32  = int32(0x5bd1e995)
	r    uint32 = uint32(24)
)

func (mur *murmur2) Sum32() uint32 {
	if mur.cached != nil {
		return *mur.cached
	}

	length := int32(len(mur.data))

	h := int32(seed ^ uint32(length))
	length4 := length / 4

	for i := int32(0); i < length4; i++ {
		i4 := i * 4
		k := int32(mur.data[i4+0]&0xff) +
			(int32(mur.data[i4+1]&0xff) << 8) +
			(int32(mur.data[i4+2]&0xff) << 16) +
			(int32(mur.data[i4+3]&0xff) << 24)
		k *= m
		k ^= int32(uint32(k) >> r)
		k *= m
		h *= m
		h ^= k
	}

	switch length % 4 {
	case 3:
		h ^= int32(mur.data[(length & ^3)+2]&0xff) << 16
		fallthrough
	case 2:
		h ^= int32(mur.data[(length & ^3)+1]&0xff) << 8
		fallthrough
	case 1:
		h ^= int32(mur.data[length & ^3] & 0xff)
		h *= m
	}

	h ^= int32(uint32(h) >> 13)
	h *= m
	h ^= int32(uint32(h) >> 15)

	cached := uint32(h)
	mur.cached = &cached
	return cached
}

func strToBalanceStrategy(str []string) ([]sarama.BalanceStrategy, error) {
	var rebalances []sarama.BalanceStrategy
	for _, i := range str {
		switch i {
		case "range":
			rebalances = append(rebalances, sarama.BalanceStrategyRange)
			break
		case "roundrobin":
			rebalances = append(rebalances, sarama.BalanceStrategyRoundRobin)
			break
		case "sticky":
			rebalances = append(rebalances, sarama.BalanceStrategySticky)
			break
		default:
			return nil, fmt.Errorf("Consumer Group RebalanceStrategy not recognised : %v", i)
		}
	}
	return rebalances, nil
}
