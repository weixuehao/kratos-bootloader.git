package saka

import (
	"context"
	"errors"
	"fmt"
	"gitee.com/weixuehao/kratos-bootloader/plugins/broker"
	"github.com/IBM/sarama"
	"github.com/go-kratos/kratos/v2/log"
	"time"
)

var errTopicNotFound = errors.New("requested topic was not found")

// TopicManager
type topicManager struct {
	admin  sarama.ClusterAdmin
	client sarama.Client
	conf   *sarama.Config
	logger *log.Helper
}

// NewTopicManager creates a new topic manager using the sarama library
func NewTopicManager(client sarama.Client, conf *sarama.Config, logger *log.Helper) (*topicManager, error) {

	if !conf.Version.IsAtLeast(sarama.V0_10_1_0) {
		return nil, fmt.Errorf("client manager is supported by brokers with version 0.10.1.0 or higher.. Version is %s", conf.Version.String())
	}

	return newTopicManager(conf, client, checkBroker, logger)
}

func newTopicManager(saramaConfig *sarama.Config, client sarama.Client, check checkFunc, logger *log.Helper) (*topicManager, error) {
	if client == nil {
		return nil, errors.New("cannot create topic manager with nil client")
	}

	activeBrokers := client.Brokers()
	if len(activeBrokers) == 0 {
		return nil, errors.New("no brokers active in current client")
	}

	broker := activeBrokers[0]
	err := check(broker, saramaConfig)
	if err != nil {
		return nil, err
	}

	admin, err := sarama.NewClusterAdminFromClient(client)
	if err != nil {
		return nil, fmt.Errorf("error creating cluster admin: %v", err)
	}

	return &topicManager{
		admin:  admin,
		client: client,
		conf:   saramaConfig,
		logger: logger,
	}, nil
}

type checkFunc func(broker *sarama.Broker, config *sarama.Config) error

func checkBroker(broker *sarama.Broker, config *sarama.Config) error {
	err := broker.Open(config)
	if err != nil {
		return fmt.Errorf("error opening broker connection: %v", err)
	}
	connected, err := broker.Connected()
	if err != nil {
		return fmt.Errorf("cannot connect to broker %s: %v", broker.Addr(), err)
	}
	if !connected {
		return fmt.Errorf("cannot connect to broker %s: not connected", broker.Addr())
	}
	return nil
}

func (m *topicManager) partitions(topic string) ([]int32, error) {
	// refresh metadata, otherwise we might get an outdated number of partitions.
	// we cannot call it for that specific topic,
	// otherwise we'd create it if auto.create.topics.enable==true, which we want to avoid
	if err := m.client.RefreshMetadata(); err != nil {
		return nil, fmt.Errorf("error refreshing metadata %v", err)
	}
	topics, err := m.client.Topics()
	if err != nil {
		return nil, err
	}
	for _, tpc := range topics {
		// topic exists, let's list the partitions.
		if tpc == topic {
			return m.client.Partitions(topic)
		}
	}
	return nil, errTopicNotFound
}

func (m *topicManager) createTopic(topic string, npar, rfactor int, timeout time.Duration, config map[string]string, replicaAssignment map[int32][]int32) error {
	m.logger.Debugf("creating topic %s with npar=%d, rfactor=%d, config=%#v", topic, npar, rfactor, config)
	topicDetail := &sarama.TopicDetail{}
	topicDetail.NumPartitions = int32(npar)
	topicDetail.ReplicationFactor = int16(rfactor)
	topicDetail.ReplicaAssignment = replicaAssignment
	topicDetail.ConfigEntries = make(map[string]*string)

	for k, v := range config {
		// configEntries is a map to `*string`, so we have to make a copy of the value
		// here or end up having the same value for all, since `v` has the same address everywhere
		value := v
		topicDetail.ConfigEntries[k] = &value
	}

	err := m.admin.CreateTopic(topic, topicDetail, false)
	if err != nil {
		return fmt.Errorf("error creating topic %s, npar=%d, rfactor=%d, config=%#v: %v",
			topic, npar, rfactor, config, err)
	}

	return m.waitForCreated(topic, timeout)
}

func (m *topicManager) ensureExists(topic string, npar, rfactor int, timeout time.Duration, config map[string]string, replicaAssignment map[int32][]int32) error {

	partitions, err := m.partitions(topic)

	if err != nil {
		if err != errTopicNotFound {
			return fmt.Errorf("error checking topic: %v", err)
		}
	}
	// no topic yet, let's create it
	if len(partitions) == 0 {
		return m.createTopic(topic,
			npar,
			rfactor,
			timeout,
			config,
			replicaAssignment)
	}
	return errors.New("Topic already exists")
}

func (m *topicManager) waitForCreated(topic string, timeout time.Duration) error {
	// no timeout defined -> no check
	if timeout == 0 {
		return nil
	}

	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()

	for ctx.Err() == nil {
		_, err := m.partitions(topic)
		switch err {
		case nil:
			return nil
		case errTopicNotFound:
			time.Sleep(time.Second)
		default:
			return fmt.Errorf("error checking topic: %w", err)
		}
	}
	return fmt.Errorf("waiting for topic %s to be created timed out", topic)
}

func (m *topicManager) CreateTopic(topic string, numPartitions, replicationFactor int, opts ...broker.Option) error {
	options := broker.NewOptionsAndApply(opts...)
	var (
		configEntries     map[string]string
		replicaAssignment map[int32][]int32
	)

	if value, ok := options.CtxValue.Value(configEntriesKey{}).(map[string]string); ok {
		configEntries = value
	}

	if value, ok := options.CtxValue.Value(replicaAssignmentKey{}).(map[int32][]int32); ok {
		replicaAssignment = value
	}
	return m.ensureExists(topic, numPartitions, replicationFactor, 0, configEntries, replicaAssignment)
}

func (m *topicManager) DeleteTopic(topic string) error {
	return m.admin.DeleteTopic(topic)
}

func (m *topicManager) CreatePartition(topic string, numPartitions int, opts ...broker.Option) error {
	options := broker.NewOptionsAndApply(opts...)

	if !m.conf.Version.IsAtLeast(sarama.V1_0_0_0) {
		return fmt.Errorf("This operation is supported by brokers with version 1.0.0 or higher. Version is %s", m.conf.Version.String())
	}
	var replicaAssignment map[int32][]int32
	var assignment [][]int32
	if value, ok := options.CtxValue.Value(replicaAssignmentKey{}).(map[int32][]int32); ok {
		replicaAssignment = value
		assignment = make([][]int32, len(replicaAssignment))

		for k, v := range replicaAssignment {
			assignment[k] = make([]int32, len(v))
			assignment[k] = v
		}
	}

	return m.admin.CreatePartitions(topic, int32(numPartitions), assignment, false)
}

func (m *topicManager) GetTopics() ([]string, error) {
	return m.client.Topics()
}

func (m *topicManager) GetPartitions(topic string) ([]int32, error) {
	return m.partitions(topic)
}

func (m *topicManager) GetOffset(topic string, partitionID int32) (int64, error) {

	return m.client.GetOffset(topic, partitionID, sarama.OffsetNewest)
}
