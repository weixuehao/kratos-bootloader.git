package saka

import (
	"gitee.com/weixuehao/kratos-bootloader/plugins/broker"
	"github.com/IBM/sarama"
)

type subscriber struct {
	k     *saBroker
	cg    sarama.ConsumerGroup
	topic []string
	opts  broker.SubscribeOptions
}

func (s *subscriber) Options() broker.SubscribeOptions {
	return s.opts
}

func (s *subscriber) Topic() []string {
	return s.topic
}

func (s *subscriber) Pause(partitions map[string][]int32) {
	s.cg.Pause(partitions)
}

func (s *subscriber) Resume(partitions map[string][]int32) {
	s.cg.Resume(partitions)
}

func (s *subscriber) Unsubscribe() error {
	if err := s.cg.Close(); err != nil {
		return err
	}

	k := s.k
	k.Lock()
	defer k.Unlock()

	for i, cg := range k.cgs {
		if cg == s.cg {
			k.cgs = append(k.cgs[:i], k.cgs[i+1:]...)
			return nil
		}
	}

	return nil
}
