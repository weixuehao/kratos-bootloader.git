package saka

import (
	"context"
	"errors"
	"gitee.com/weixuehao/kratos-bootloader/plugins/broker"
	"github.com/IBM/sarama"
	"github.com/go-kratos/kratos/v2/log"
	"go.opentelemetry.io/otel/attribute"
	semConv "go.opentelemetry.io/otel/semconv/v1.12.0"
	"go.opentelemetry.io/otel/trace"
	"strconv"
	"strings"
	"sync"
	"time"
)

const (
	defaultAddr = "127.0.0.1:9092"
)

// sarama kafka broker
type saBroker struct {
	sync.RWMutex
	addrs        []string
	logger       *log.Helper
	client       sarama.Client
	conf         *sarama.Config
	publisher    *publisher
	cgs          []sarama.ConsumerGroup
	topicManager *topicManager
	connected    bool
	//scMutex   sync.Mutex
	opts broker.Options

	producerTracer *broker.Tracer
	consumerTracer *broker.Tracer
}

func NewSaramaKafka(opts ...broker.Option) broker.Broker {
	options := broker.NewOptionsAndApply(opts...)
	b := &saBroker{
		opts: options,
	}
	return b
}

func (b *saBroker) Name() string {
	return "kafka"
}

func (b *saBroker) Address() string {
	if len(b.opts.Addrs) > 0 {
		return strings.Join(b.opts.Addrs, ",")
	}
	return defaultAddr
}

func (b *saBroker) Options() broker.Options {
	return b.opts
}

func (b *saBroker) Init(opts ...broker.Option) error {
	b.opts.Apply(opts...)
	if b.opts.Logger == nil {
		b.opts.Logger = log.NewHelper(log.GetLogger())
	}
	var addrs []string
	for _, addr := range b.opts.Addrs {
		if len(addr) == 0 {
			continue
		}
		addrs = append(addrs, addr)
	}
	if len(addrs) == 0 {
		addrs = []string{defaultAddr}
	}
	if len(b.opts.Tracings) > 0 {
		b.producerTracer = broker.NewTracer(trace.SpanKindProducer, "kafka-producer", b.opts.Tracings...)
		b.consumerTracer = broker.NewTracer(trace.SpanKindConsumer, "kafka-consumer", b.opts.Tracings...)
	}
	b.Lock()
	b.opts.Addrs = addrs
	b.logger = b.opts.Logger
	b.publisher = newPublisher()
	config := sarama.NewConfig()
	b.conf = getClusterConfig(config, b.opts, b.logger)
	b.Unlock()
	return nil
}

func (b *saBroker) Connect() error {
	b.RLock()
	if b.connected {
		b.RUnlock()
		return nil
	}
	b.RUnlock()

	b.RLock()
	if b.client != nil {
		b.RUnlock()
		return nil
	}
	b.RUnlock()

	kAddrs := make([]string, 0, len(b.opts.Addrs))
	for _, addr := range b.opts.Addrs {
		kAddrs = append(kAddrs, addr)
	}

	if len(kAddrs) == 0 {
		return errors.New("no available addrs")
	}

	c, err := sarama.NewClient(kAddrs, b.conf)
	if err != nil {
		return err
	}

	b.Lock()
	b.client = c
	b.cgs = make([]sarama.ConsumerGroup, 0)
	b.opts.Addrs = kAddrs
	b.addrs = kAddrs
	b.connected = true
	b.Unlock()

	return nil
}

func (b *saBroker) Disconnect() error {
	b.RLock()
	if !b.connected {
		b.RUnlock()
		return nil
	}
	b.RUnlock()

	b.Lock()
	defer b.Unlock()
	b.publisher.close()
	for _, consumer := range b.cgs {
		consumer.Close()
	}
	b.cgs = nil
	if err := b.client.Close(); err != nil {
		return err
	}
	b.connected = false
	time.Sleep(2 * time.Second)
	return nil
}

func (b *saBroker) Publish(topic string, msg *broker.Message, opts ...broker.PublishOption) (broker.Publisher, error) {

	options := broker.NewPublishOptionsAndApply(opts...)
	enableOneTopicOneWriter := true
	if value, ok := options.CtxValue.Value(enableOneTopicOneWriterKey{}).(bool); ok {
		enableOneTopicOneWriter = value
	}

	b.publisher.enableOneTopicOneWriter = enableOneTopicOneWriter
	b.publisher.async = options.Async

	if b.publisher.async {
		return b.asyncProducer(topic, msg, options)
	} else {
		return b.syncProducer(topic, msg, options)
	}

}

func (b *saBroker) syncProducer(topic string, msg *broker.Message, pubopts broker.PublishOptions) (broker.Publisher, error) {

	var kafkaHeader []sarama.RecordHeader
	for k, v := range msg.Headers {
		kafkaHeader = append(kafkaHeader, sarama.RecordHeader{Key: []byte(k), Value: v})
	}
	var key sarama.Encoder
	if msg.Key != "" {
		key = sarama.ByteEncoder(msg.Key)
	}
	var produceMsg = &sarama.ProducerMessage{
		Topic:     topic,
		Key:       key,
		Value:     sarama.ByteEncoder(msg.Value),
		Headers:   kafkaHeader,
		Timestamp: msg.Timestamp,
		Partition: msg.Partition,
		Offset:    msg.Offset,
		Metadata:  msg,
	}

	var writer sarama.SyncProducer
	var ok bool
	if b.publisher.enableOneTopicOneWriter {
		b.Lock()
		writer, ok = b.publisher.sps[topic]
		if !ok {
			producer, err := b.publisher.createSyncProducer(b.addrs, b.logger, b.conf, pubopts)
			if err != nil {
				return nil, err
			}
			b.publisher.sps[topic] = producer
			writer = producer
		}
		b.Unlock()
	} else {
		b.Lock()
		if b.publisher.sp == nil {
			producer, err := b.publisher.createSyncProducer(b.addrs, b.logger, b.conf, pubopts)
			if err != nil {
				return nil, err
			}
			b.publisher.sp = producer
			writer = producer
		} else {
			writer = b.publisher.sp
		}
		b.Unlock()
	}
	enableTransaction := false
	if value, ok := pubopts.CtxValue.Value(enableTransactionKey{}).(bool); ok {
		enableTransaction = value
	}

	var (
		err    error
		parti  int32
		offset int64
	)
	pub := &publication{}
	_, span := b.startProducerSpan(pubopts.Context, msg)
	defer b.finishProducerSpan(span, pub.msg.Partition, pub.msg.Offset, err)

	if enableTransaction {

		err = writer.BeginTxn()
		if err != nil {
			return pub, err
		}

		parti, offset, err = writer.SendMessage(produceMsg)
		err = writer.CommitTxn()
		if err != nil {
			return pub, err
		}
		pub.topic = topic
		pub.msg = msg
		pub.syncProducer = writer
		pub.msg.Partition = parti
		pub.msg.Offset = offset
		return pub, err
	}

	parti, offset, err = writer.SendMessage(produceMsg)
	//if err != nil {
	//	b.Lock()
	//	if err = writer.Close(); err != nil {
	//		b.Unlock()
	//	}
	//	if b.publisher.enableOneTopicOneWriter {
	//		delete(b.publisher.sps, topic)
	//	} else {
	//		b.publisher.sp = nil
	//	}
	//	b.Unlock()
	//}
	pub.topic = topic
	pub.msg = msg
	pub.syncProducer = writer
	pub.msg.Partition = parti
	pub.msg.Offset = offset
	return pub, err
}

func (b *saBroker) asyncProducer(topic string, msg *broker.Message, pubopts broker.PublishOptions) (broker.Publisher, error) {
	var kafkaHeader []sarama.RecordHeader
	for k, v := range msg.Headers {
		kafkaHeader = append(kafkaHeader, sarama.RecordHeader{Key: []byte(k), Value: v})
	}
	var key sarama.Encoder
	if msg.Key != "" {
		key = sarama.ByteEncoder(msg.Key)
	}
	var produceMsg = &sarama.ProducerMessage{
		Topic:     topic,
		Key:       key,
		Value:     sarama.ByteEncoder(msg.Value),
		Headers:   kafkaHeader,
		Timestamp: msg.Timestamp,
		Partition: msg.Partition,
		Offset:    msg.Offset,
		Metadata:  msg,
	}

	var writer sarama.AsyncProducer
	var ok bool
	if b.publisher.enableOneTopicOneWriter {
		b.Lock()
		writer, ok = b.publisher.aps[topic]
		if !ok {
			producer, err := b.publisher.createAsyncProducer(b.addrs, b.logger, b.conf, pubopts)
			if err != nil {
				return nil, err
			}
			b.publisher.aps[topic] = producer
			writer = producer
		}
		b.Unlock()
	} else {
		b.Lock()
		if b.publisher.ap == nil {
			producer, err := b.publisher.createAsyncProducer(b.addrs, b.logger, b.conf, pubopts)
			if err != nil {
				return nil, err
			}
			b.publisher.ap = producer
			writer = producer
		} else {
			writer = b.publisher.ap
		}
		b.Unlock()
	}
	enableTransaction := false
	if value, ok := pubopts.CtxValue.Value(enableTransactionKey{}).(bool); ok {
		enableTransaction = value
	}

	var (
		err error
	)
	pub := &publication{}
	_, span := b.startProducerSpan(pubopts.Context, msg)
	defer b.finishProducerSpan(span, pub.msg.Partition, pub.msg.Offset, err)

	if enableTransaction {
		err = writer.BeginTxn()
		if err != nil {
			return nil, err
		}
		writer.Input() <- produceMsg
		err = writer.CommitTxn()
		if err != nil {
			return nil, err
		}
		pub.topic = topic
		pub.msg = msg
		pub.asyncProducer = writer
		return pub, nil
	}

	writer.Input() <- produceMsg
	pub.topic = topic
	pub.msg = msg
	pub.asyncProducer = writer
	return pub, nil
}

func (b *saBroker) Subscribe(topics []string, handler broker.Handler, opts ...broker.SubscribeOption) (broker.Subscriber, error) {

	options := broker.NewSubscribeOptionsAndApply(opts...)

	config := getClusterConfig(b.conf, options, b.logger)

	cg, err := sarama.NewConsumerGroup(b.addrs, options.Queue, config)
	if err != nil {
		return nil, err
	}
	b.Lock()
	b.cgs = append(b.cgs, cg)
	b.Unlock()

	if handler == nil {
		options.AutoAck = false
	}

	h := &consumerGroupHandler{
		logger:         b.logger,
		handler:        handler,
		subopts:        options,
		opts:           b.opts,
		cg:             cg,
		consumerTracer: b.consumerTracer,
	}

	go func() {
		for {
			err, ok := <-cg.Errors()
			if !ok {
				return
			}
			if err != nil {
				b.logger.Errorf("error during execution of consumer group: %v", err)
			}
		}
	}()

	go func() {

		for {
			err := cg.Consume(options.Context, topics, h)
			switch err {
			case sarama.ErrClosedConsumerGroup:
				return
			case nil:
				continue
			default:
				b.logger.Errorf("error consuming from consumer group: %v", err)
			}

			select {
			case <-time.After(1 * time.Second):
				b.logger.Infof("Consumer group returned, Rebalancing.")
			case <-options.Context.Done():
				b.logger.Infof("Consumer group cancelled. Stopping")
				return
			}
		}
	}()
	return &subscriber{k: b, cg: cg, opts: options, topic: topics}, nil
}

func (b *saBroker) ClientManager() (broker.ClientManager, error) {
	topicManager, err := NewTopicManager(b.client, b.conf, b.logger)
	if err != nil {
		return nil, err
	}
	b.Lock()
	b.topicManager = topicManager
	b.Unlock()
	return topicManager, nil
}

func (b *saBroker) startProducerSpan(ctx context.Context, msg *broker.Message) (context.Context, trace.Span) {
	if b.producerTracer == nil {
		return ctx, nil
	}

	carrier := broker.NewMessageCarrier(msg)

	attrs := []attribute.KeyValue{
		semConv.MessagingSystemKey.String("kafka"),
		semConv.MessagingDestinationKindTopic,
		semConv.MessagingDestinationKey.String(msg.Topic),
	}

	var span trace.Span
	ctx, span = b.producerTracer.Start(ctx, carrier, attrs...)

	return ctx, span
}

func (b *saBroker) finishProducerSpan(span trace.Span, partition int32, offset int64, err error) {
	if b.producerTracer == nil {
		return
	}

	attrs := []attribute.KeyValue{
		semConv.MessagingMessageIDKey.String(strconv.FormatInt(offset, 10)),
		semConv.MessagingKafkaPartitionKey.Int64(int64(partition)),
	}

	b.producerTracer.End(context.Background(), span, err, attrs...)
}
