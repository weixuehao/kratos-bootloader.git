// Package broker is an interface used for asynchronous messaging
package broker

import (
	"context"
	"time"
)

type Message struct {
	Key       string
	Value     []byte
	Headers   map[string][]byte
	Timestamp time.Time
	Topic     string
	Offset    int64
	Partition int32
}

// Broker is an interface used for asynchronous messaging.
type Broker interface {
	Name() string
	Options() Options
	Address() string
	Init(...Option) error
	Connect() error
	Disconnect() error
	ClientManager() (ClientManager, error)
	Publish(topic string, msg *Message, opts ...PublishOption) (Publisher, error)
	Subscribe(topics []string, handler Handler, opts ...SubscribeOption) (Subscriber, error)
}

// Handler is used to process messages via a subscription of a topic.
// The handler is passed a subscription interface which contains the
// message and optional Ack method to acknowledge receipt of the message.
type Handler func(context.Context, Event) error

// Subscriber is a convenience return type for the Subscribe method
type Subscriber interface {
	Options() SubscribeOptions
	Topic() []string
	// Pause suspends fetching from the requested partitions. Future calls to the broker will not return any
	// records from these partitions until they have been resumed using Resume()/ResumeAll().
	// Note that this method does not affect partition subscription.
	// In particular, it does not cause a group rebalance when automatic assignment is used.
	Pause(partitions map[string][]int32)
	// Resume resumes specified partitions which have been paused with Pause()/PauseAll().
	// New calls to the broker will return records from these partitions if there are any to be fetched.
	Resume(partitions map[string][]int32)
	Unsubscribe() error
}

// Event is given to a subscription handler for processing
type Event interface {
	Topic() string
	Message() *Message
	Ack(metadata string) error
	Claims() map[string][]int32
	Commit()
	Context() context.Context
	Error() error
}

// Publisher is a convenience return type for the Publish method
type Publisher interface {
	Topic() string
	Message() *Message
	Error() error
	Close() error
}

type ClientManager interface {
	CreateTopic(topic string, npar, rfactor int, opts ...Option) error
	DeleteTopic(topic string) error
	CreatePartition(topic string, npar int, opts ...Option) error
	GetTopics() ([]string, error)
	GetPartitions(topic string) ([]int32, error)
	GetOffset(topic string, partitionID int32) (int64, error)
}
