module gitee.com/weixuehao/kratos-bootloader/plugins/middleware/auth

go 1.21

toolchain go1.22.1

require (
	github.com/casbin/casbin/v2 v2.101.0
	github.com/go-kratos/kratos/v2 v2.8.2
)

require (
	github.com/bmatcuk/doublestar/v4 v4.6.1 // indirect
	github.com/casbin/govaluate v1.2.0 // indirect
	github.com/golang/protobuf v1.5.4 // indirect
	golang.org/x/sys v0.18.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20240102182953-50ed04b92917 // indirect
	google.golang.org/grpc v1.61.1 // indirect
	google.golang.org/protobuf v1.33.0 // indirect
)
