// Copyright The OpenTelemetry Authors
// SPDX-License-Identifier: Apache-2.0

package zipkin // import "go.opentelemetry.io/otel/exporters/zipkin"

import (
	"context"
	"fmt"
	"github.com/openzipkin/zipkin-go/model"
	"github.com/openzipkin/zipkin-go/reporter"
	"log"
	"sync"

	"github.com/go-logr/logr"
	"github.com/go-logr/stdr"

	sdktrace "go.opentelemetry.io/otel/sdk/trace"
)

const (
	defaultCollectorURL = "http://localhost:9411/api/v2/spans"
	Zipkin_info         = "zipkin_succ"
	Zipkin_error        = "zipkin_error"
)

type CustomReporter interface {
	Send(model.SpanModel, bool) // Send Span data to the reporter
	Close() error               // Close the reporter
}

// Exporter exports spans to the zipkin collector.
type Exporter struct {
	openReporter   reporter.Reporter
	customReporter CustomReporter
	logger         logr.Logger

	stoppedMu sync.RWMutex
	stopped   bool
}

var _ sdktrace.SpanExporter = &Exporter{}

var emptyLogger = logr.Logger{}

// Options contains configuration for the exporter.
type config struct {
	openReporter   reporter.Reporter
	customReporter CustomReporter
	logger         logr.Logger
}

// Option defines a function that configures the exporter.
type Option interface {
	apply(config) config
}

type optionFunc func(config) config

func (fn optionFunc) apply(cfg config) config {
	return fn(cfg)
}

// WithLogger configures the exporter to use the passed logger.
// WithLogger and WithLogr will overwrite each other.
func WithLogger(logger *log.Logger) Option {
	return WithLogr(stdr.New(logger))
}

// WithLogr configures the exporter to use the passed logr.Logger.
// WithLogr and WithLogger will overwrite each other.
func WithLogr(logger logr.Logger) Option {
	return optionFunc(func(cfg config) config {
		cfg.logger = logger
		return cfg
	})
}

// WithReporter configures the custom reporter can be used to provide the Zipkin Tracer.
// implementations to publish Zipkin Span data.
func WithCustomReporter(reporter CustomReporter) Option {
	return optionFunc(func(cfg config) config {
		cfg.customReporter = reporter
		return cfg
	})
}

// WithReporter configures the open reporter can be used to provide the Zipkin Tracer with custom.
// implementations to publish Zipkin Span data.
func WithOpenReporter(reporter reporter.Reporter) Option {
	return optionFunc(func(cfg config) config {
		cfg.openReporter = reporter
		return cfg
	})
}

// New creates a new Zipkin exporter.
func New(opts ...Option) (*Exporter, error) {
	cfg := config{}
	for _, opt := range opts {
		cfg = opt.apply(cfg)
	}
	if cfg.customReporter == nil && cfg.openReporter == nil {
		return nil, fmt.Errorf("the reporter cannot be nil")
	}
	return &Exporter{
		customReporter: cfg.customReporter,
		openReporter:   cfg.openReporter,
		logger:         cfg.logger,
	}, nil
}

// ExportSpans exports spans to a Zipkin receiver.
func (e *Exporter) ExportSpans(ctx context.Context, spans []sdktrace.ReadOnlySpan) error {
	e.stoppedMu.RLock()
	stopped := e.stopped
	e.stoppedMu.RUnlock()
	if stopped {
		e.logf("exporter stopped, not exporting span batch")
		return nil
	}

	if len(spans) == 0 {
		e.logf("no spans to export")
		return nil
	}
	models := SpanModels(spans)
	e.logf("about to send a spanModel to zipkin with body %v", models)

	for _, span := range models {
		bflag := true
		for key, _ := range span.Tags {
			if key == Zipkin_info || key == Zipkin_error {
				if key == Zipkin_info {
					bflag = true //成功的
				} else {
					bflag = false //失败的链路
				}
			}
		}

		if e.openReporter != nil {
			e.openReporter.Send(span)
		} else {
			e.customReporter.Send(span, bflag)
		}

	}
	return nil
}

// Shutdown stops the exporter flushing any pending exports.
func (e *Exporter) Shutdown(ctx context.Context) error {
	e.stoppedMu.Lock()
	e.stopped = true
	e.stoppedMu.Unlock()

	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
	}
	return nil
}

func (e *Exporter) logf(format string, args ...interface{}) {
	if e.logger != emptyLogger {
		e.logger.Info(fmt.Sprintf(format, args...))
	}
}

func (e *Exporter) errf(format string, args ...interface{}) error {
	e.logf(format, args...)
	return fmt.Errorf(format, args...)
}

// MarshalLog is the marshaling function used by the logging system to represent this Exporter.
func (e *Exporter) MarshalLog() interface{} {
	return struct {
		Type string
	}{
		Type: "zipkin",
	}
}
