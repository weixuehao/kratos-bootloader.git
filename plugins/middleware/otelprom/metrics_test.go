package otelprom

import (
	"github.com/go-kratos/kratos/v2/transport/http"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"go.opentelemetry.io/otel/exporters/prometheus"
	"go.opentelemetry.io/otel/metric"
	sdkmetric "go.opentelemetry.io/otel/sdk/metric"
	"testing"
)

var (
	_metricRequests metric.Int64Counter
	_metricSeconds  metric.Float64Histogram
)

// Detailed reference https://github.com/go-kratos/examples/tree/main/metrics
func initMetrics() {
	exporter, err := prometheus.New()
	if err != nil {
		panic(err)
	}
	provider := sdkmetric.NewMeterProvider(sdkmetric.WithReader(exporter))
	meter := provider.Meter("kratos")

	_metricRequests, err = DefaultRequestsCounter(meter, DefaultHttpServerRequestsCounterName)
	if err != nil {
		panic(err)
	}

	_metricSeconds, err = DefaultSecondsHistogram(meter, DefaultHttpServerSecondsHistogramName)
	if err != nil {
		panic(err)
	}
}

func TestMetrics(t *testing.T) {
	initMetrics()
	// grpc service
	//grpcSrv := grpc.NewServer(
	//	grpc.Address(":9000"),
	//	grpc.Middleware(
	//		Server(
	//			WithSeconds(_metricSeconds),
	//			WithRequests(_metricRequests),
	//		),
	//	),
	//)

	// http service
	httpSrv := http.NewServer(
		http.Address(":8000"),
		http.Middleware(
			Server(
				WithSeconds(_metricSeconds),
				WithRequests(_metricRequests),
			),
		),
	)
	httpSrv.Handle("/metrics", promhttp.Handler())
}
