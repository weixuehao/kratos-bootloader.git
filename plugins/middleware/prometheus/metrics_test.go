package prometheus

import (
	prom "gitee.com/weixuehao/kratos-bootloader/plugins/metrics/prometheus"
	"github.com/go-kratos/kratos/v2/transport/http"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"testing"
)

func TestMetrics(t *testing.T) {

	_metricSeconds := prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: "http_server",
		Subsystem: "requests",
		Name:      "duration_ms",
		Help:      "http server requests duration(ms).",
		Buckets:   []float64{5, 50, 100, 250, 500, 1000, 5000, 10000},
	}, []string{"kind", "operation", "path", "method"})

	_metricRequests := prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "http_server",
		Subsystem: "requests",
		Name:      "code_total",
		Help:      "http server requests error count.",
	}, []string{"kind", "operation", "code", "reason", "path", "method"})

	prometheus.MustRegister(_metricSeconds, _metricRequests)

	// grpc service
	//grpcSrv := grpc.NewServer(
	//	grpc.Address(":9000"),
	//	grpc.Middleware(
	//		Server(
	//			WithSeconds(prom.NewHistogram(_metricSeconds)),
	//			WithRequests(prom.NewCounter(_metricRequests)),
	//		),
	//	),
	//)

	// http service
	httpSrv := http.NewServer(
		http.Address(":8000"),
		http.Middleware(
			Server(
				WithSeconds(prom.NewHistogram(_metricSeconds)),
				WithRequests(prom.NewCounter(_metricRequests)),
			),
		),
	)
	httpSrv.Handle("/metrics", promhttp.Handler())
}
