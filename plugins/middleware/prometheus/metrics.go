package prometheus

import (
	"context"
	"gitee.com/weixuehao/kratos-bootloader/plugins/metrics"
	prom "gitee.com/weixuehao/kratos-bootloader/plugins/metrics/prometheus"
	"github.com/go-kratos/kratos/v2/errors"
	"github.com/go-kratos/kratos/v2/middleware"
	"github.com/go-kratos/kratos/v2/transport"
	"github.com/go-kratos/kratos/v2/transport/http"
	"github.com/go-kratos/kratos/v2/transport/http/status"
	"github.com/prometheus/client_golang/prometheus"
	"google.golang.org/grpc/codes"
	"strconv"
	"time"
)

// Option is metrics option.
type Option func(*options)

// WithRequests with requests counter.
func WithRequests(c metrics.Counter) Option {
	return func(o *options) {
		o.requests = c
	}
}

// WithSeconds with seconds histogram.
func WithSeconds(c metrics.Observer) Option {
	return func(o *options) {
		o.seconds = c
	}
}

type options struct {
	// counter: <client/server>_requests_code_total{kind, operation, code, reason}
	requests metrics.Counter
	// histogram: <client/server>_requests_seconds_bucket{kind, operation}
	seconds metrics.Observer
}

var (
	_metricServerRequests *prometheus.CounterVec
	_metricServerSeconds  *prometheus.HistogramVec
	_metricClientRequests *prometheus.CounterVec
	_metricClientSeconds  *prometheus.HistogramVec
)

func init() {
	_metricServerSeconds = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: "http_server",
		Subsystem: "requests",
		Name:      "duration_ms",
		Help:      "http server requests duration(ms).",
		Buckets:   []float64{5, 50, 100, 250, 500, 1000, 5000, 10000},
	}, []string{"kind", "operation", "path", "method"})

	_metricServerRequests = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "http_server",
		Subsystem: "requests",
		Name:      "code_total",
		Help:      "http server requests error count.",
	}, []string{"kind", "operation", "code", "reason", "path", "method"})

	_metricClientSeconds = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: "http_client",
		Subsystem: "requests",
		Name:      "duration_ms",
		Help:      "http server requests duration(ms).",
		Buckets:   []float64{5, 50, 100, 250, 500, 1000, 5000, 10000},
	}, []string{"kind", "operation", "path", "method"})

	_metricClientRequests = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "http_client",
		Subsystem: "requests",
		Name:      "code_total",
		Help:      "http server requests error count.",
	}, []string{"kind", "operation", "code", "reason", "path", "method"})

	prometheus.MustRegister(_metricServerRequests, _metricServerSeconds, _metricClientRequests, _metricClientSeconds)
}

// Server is middleware server-side metrics.
func Server(opts ...Option) middleware.Middleware {
	op := options{
		seconds:  prom.NewHistogram(_metricServerSeconds),
		requests: prom.NewCounter(_metricServerRequests),
	}
	for _, o := range opts {
		o(&op)
	}
	return func(handler middleware.Handler) middleware.Handler {
		return func(ctx context.Context, req interface{}) (interface{}, error) {
			var (
				code      int
				reason    string
				kind      string
				operation string
				path      string
				method    string
			)
			// default code
			code = status.FromGRPCCode(codes.OK)
			startTime := time.Now()
			if info, ok := transport.FromServerContext(ctx); ok {
				kind = info.Kind().String()
				operation = info.Operation()
				if tr, ok := info.(*http.Transport); ok {
					path = tr.PathTemplate()
					method = tr.Request().Method
				}
			}
			reply, err := handler(ctx, req)
			if se := errors.FromError(err); se != nil {
				code = int(se.Code)
				reason = se.Reason
			}
			if op.requests != nil {
				op.requests.With(kind, operation, strconv.Itoa(code), reason, path, method).Inc()
			}
			if op.seconds != nil {
				op.seconds.With(kind, operation, path, method).Observe(time.Since(startTime).Seconds())
			}
			return reply, err
		}
	}
}

// Client is middleware client-side metrics.
func Client(opts ...Option) middleware.Middleware {
	op := options{
		seconds:  prom.NewHistogram(_metricClientSeconds),
		requests: prom.NewCounter(_metricClientRequests),
	}
	for _, o := range opts {
		o(&op)
	}
	return func(handler middleware.Handler) middleware.Handler {
		return func(ctx context.Context, req interface{}) (interface{}, error) {
			var (
				code      int
				reason    string
				kind      string
				operation string
				path      string
				method    string
			)
			// default code
			code = status.FromGRPCCode(codes.OK)
			startTime := time.Now()
			if info, ok := transport.FromClientContext(ctx); ok {
				kind = info.Kind().String()
				operation = info.Operation()
				if tr, ok := info.(*http.Transport); ok {
					path = tr.PathTemplate()
					method = tr.Request().Method
				}
			}
			reply, err := handler(ctx, req)
			if se := errors.FromError(err); se != nil {
				code = int(se.Code)
				reason = se.Reason
			}
			if op.requests != nil {
				op.requests.With(kind, operation, strconv.Itoa(code), reason, path, method).Inc()
			}
			if op.seconds != nil {
				op.seconds.With(kind, operation, path, method).Observe(time.Since(startTime).Seconds())
			}
			return reply, err
		}
	}
}
