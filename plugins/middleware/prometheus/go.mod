module gitee.com/weixuehao/kratos-bootloader/plugins/middleware/prometheus

go 1.22

toolchain go1.23.0

require (
	gitee.com/weixuehao/kratos-bootloader v1.0.0
	gitee.com/weixuehao/kratos-bootloader/plugins/metrics/prometheus v0.0.0-00010101000000-000000000000
	github.com/go-kratos/kratos/v2 v2.8.2
	github.com/prometheus/client_golang v1.20.4
	google.golang.org/grpc v1.65.0
)

require (
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/cespare/xxhash/v2 v2.3.0 // indirect
	github.com/go-kratos/aegis v0.2.0 // indirect
	github.com/go-playground/form/v4 v4.2.0 // indirect
	github.com/google/uuid v1.6.0 // indirect
	github.com/gorilla/mux v1.8.1 // indirect
	github.com/klauspost/compress v1.17.9 // indirect
	github.com/kr/text v0.2.0 // indirect
	github.com/munnerz/goautoneg v0.0.0-20191010083416-a7dc8b61c822 // indirect
	github.com/prometheus/client_model v0.6.1 // indirect
	github.com/prometheus/common v0.55.0 // indirect
	github.com/prometheus/procfs v0.15.1 // indirect
	golang.org/x/sys v0.22.0 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20240528184218-531527333157 // indirect
	google.golang.org/protobuf v1.34.2 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)

replace gitee.com/weixuehao/kratos-bootloader => ../../../

replace gitee.com/weixuehao/kratos-bootloader/plugins/metrics/prometheus => ../../metrics/prometheus
