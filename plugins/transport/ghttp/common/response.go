package common

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type Result struct {
	Code    int         `json:"code"`
	Message string      `json:"message"`
	Data    interface{} `json:"data"`
}

const (
	ERROR   = 7
	SUCCESS = 0
)

func ResGenerate(code int, data interface{}, msg string, c *gin.Context) {
	// 开始时间
	c.JSON(http.StatusOK, Result{
		code,
		msg,
		data,
	})
}

func Ok(c *gin.Context) {
	ResGenerate(SUCCESS, map[string]interface{}{}, "操作成功", c)
}

func OkWithMessage(message string, c *gin.Context) {
	ResGenerate(SUCCESS, map[string]interface{}{}, message, c)
}

func OkWithData(data interface{}, c *gin.Context) {
	ResGenerate(SUCCESS, data, "成功", c)
}

func OkWithDetailed(data interface{}, message string, c *gin.Context) {
	ResGenerate(SUCCESS, data, message, c)
}

func Fail(c *gin.Context) {
	ResGenerate(ERROR, map[string]interface{}{}, "操作失败", c)
}

func FailWithMessage(message string, c *gin.Context) {
	ResGenerate(ERROR, map[string]interface{}{}, message, c)
}

func NoAuth(message string, c *gin.Context) {
	c.JSON(http.StatusUnauthorized, Result{
		7,
		message,
		nil,
	})
}

func FailWithDetailed(data interface{}, message string, c *gin.Context) {
	ResGenerate(ERROR, data, message, c)
}
