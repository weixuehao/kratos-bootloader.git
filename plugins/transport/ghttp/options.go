package ghttp

import (
	"crypto/tls"
	"github.com/gin-gonic/gin"
	"github.com/go-kratos/kratos/v2/middleware"
	"net"
	"net/http"
	"net/url"
	"time"

	kHttp "github.com/go-kratos/kratos/v2/transport/http"

	"go.opentelemetry.io/contrib/instrumentation/github.com/gin-gonic/gin/otelgin"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/trace"
)

type ServerOption func(*Server)

// WithGinOptionFunc gin.OptionFunc
func WithGinOptionFunc(ginOpt ...gin.OptionFunc) ServerOption {
	return func(s *Server) {
		s.ginOpt = append(s.ginOpt, ginOpt...)
	}
}

func WithHttpServer(srv *http.Server) ServerOption {
	return func(s *Server) {
		s.Server = srv
	}
}

// Network with server network.
func Network(network string) ServerOption {
	return func(s *Server) {
		s.network = network
	}
}

// Address with server address.
func Address(addr string) ServerOption {
	return func(s *Server) {
		s.address = addr
	}
}

// Endpoint with server address.
func Endpoint(endpoint *url.URL) ServerOption {
	return func(s *Server) {
		s.endpoint = endpoint
	}
}

// Timeout with server timeout.
func Timeout(timeout time.Duration) ServerOption {
	return func(s *Server) {
		s.timeout = timeout
	}
}

// Middleware with service middleware option.
func Middleware(m ...middleware.Middleware) ServerOption {
	return func(o *Server) {
		o.middleware.Use(m...)
	}
}

// Filter with HTTP middleware option.
func Filter(filters ...FilterFunc) ServerOption {
	return func(o *Server) {
		o.filters = filters
	}
}

// RequestVarsDecoder with request decoder.
func RequestVarsDecoder(dec kHttp.DecodeRequestFunc) ServerOption {
	return func(o *Server) {
		o.decVars = dec
	}
}

// RequestQueryDecoder with request decoder.
func RequestQueryDecoder(dec kHttp.DecodeRequestFunc) ServerOption {
	return func(o *Server) {
		o.decQuery = dec
	}
}

// RequestDecoder with request decoder.
func RequestDecoder(dec kHttp.DecodeRequestFunc) ServerOption {
	return func(o *Server) {
		o.decBody = dec
	}
}

// ResponseEncoder with response encoder.
func ResponseEncoder(en kHttp.EncodeResponseFunc) ServerOption {
	return func(o *Server) {
		o.enc = en
	}
}

// ErrorEncoder with error encoder.
func ErrorEncoder(en kHttp.EncodeErrorFunc) ServerOption {
	return func(o *Server) {
		o.ene = en
	}
}

// TLSConfig with TLS config.
func TLSConfig(c *tls.Config) ServerOption {
	return func(o *Server) {
		o.tlsConf = c
	}
}

// Listener with server lis
func Listener(lis net.Listener) ServerOption {
	return func(s *Server) {
		s.lis = lis
	}
}

// WithStrictSlash is with mux's StrictSlash
// If true, when the path pattern is "/path/", accessing "/path" will
// redirect to the former and vice versa.
func WithStrictSlash(strictSlash bool) ServerOption {
	return func(s *Server) {
		s.Engine.RedirectTrailingSlash = strictSlash
	}
}

// WithCustomTracer 注入链路追踪器
func WithTracer(provider trace.TracerProvider, propagator propagation.TextMapPropagator) ServerOption {
	return func(s *Server) {
		s.Engine.Use(otelgin.Middleware("kgin",
			otelgin.WithTracerProvider(provider),
			otelgin.WithPropagators(propagator),
		))
	}
}
