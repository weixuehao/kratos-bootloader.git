package ghttp

import (
	"context"
	"github.com/go-kratos/kratos/v2/errors"
	"github.com/go-kratos/kratos/v2/transport/http"
	"github.com/go-kratos/kratos/v2/transport/http/binding"
	"google.golang.org/protobuf/types/known/emptypb"
	"google.golang.org/protobuf/types/known/fieldmaskpb"
)

// 验证密码结果码
type VerifyPasswordResult int32

const (
	VerifyPasswordResult_SUCCESS            VerifyPasswordResult = 0 // 验证成功
	VerifyPasswordResult_ACCOUNT_NOT_EXISTS VerifyPasswordResult = 1 // 账号不存在
	VerifyPasswordResult_WRONG_PASSWORD     VerifyPasswordResult = 2 // 密码错误
	VerifyPasswordResult_FREEZE             VerifyPasswordResult = 3 // 已冻结
	VerifyPasswordResult_DELETED            VerifyPasswordResult = 4 // 已删除
)

// 用户权限
type UserAuthority int32

// 用户
type User struct {
	Id          uint32         `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	UserName    *string        `protobuf:"bytes,2,opt,name=userName,proto3,oneof" json:"userName,omitempty"`
	NickName    *string        `protobuf:"bytes,3,opt,name=nickName,proto3,oneof" json:"nickName,omitempty"`
	Email       *string        `protobuf:"bytes,4,opt,name=email,proto3,oneof" json:"email,omitempty"`
	Avatar      *string        `protobuf:"bytes,5,opt,name=avatar,proto3,oneof" json:"avatar,omitempty"`
	Description *string        `protobuf:"bytes,6,opt,name=description,proto3,oneof" json:"description,omitempty"`
	Password    *string        `protobuf:"bytes,7,opt,name=password,proto3,oneof" json:"password,omitempty"`
	CreateTime  *string        `protobuf:"bytes,8,opt,name=createTime,proto3,oneof" json:"createTime,omitempty"`
	UpdateTime  *string        `protobuf:"bytes,9,opt,name=updateTime,proto3,oneof" json:"updateTime,omitempty"`
	Status      *string        `protobuf:"bytes,10,opt,name=status,proto3,oneof" json:"status,omitempty"`
	RoleId      *uint32        `protobuf:"varint,11,opt,name=roleId,proto3,oneof" json:"roleId,omitempty"`                                          // 角色ID
	CreatorId   *uint32        `protobuf:"varint,12,opt,name=creatorId,proto3,oneof" json:"creatorId,omitempty"`                                    // 创建者ID
	Authority   *UserAuthority `protobuf:"varint,13,opt,name=authority,proto3,enum=user.service.v1.UserAuthority,oneof" json:"authority,omitempty"` // 权限
}

// 获取用户数据 - 请求
type GetUserRequest struct {
	Id uint32 `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
}

type GetUserByUserNameRequest struct {
	UserName string `protobuf:"bytes,1,opt,name=userName,proto3" json:"userName,omitempty"`
}

// 获取用户列表 - 答复
type ListUserResponse struct {
	Items []*User `protobuf:"bytes,1,rep,name=items,proto3" json:"items,omitempty"`
	Total int32   `protobuf:"varint,2,opt,name=total,proto3" json:"total,omitempty"`
}

type PagingRequest struct {
	// 当前页码
	Page *int32 `protobuf:"varint,1,opt,name=page,proto3,oneof" json:"page,omitempty"`
	// 每页的行数
	PageSize *int32 `protobuf:"varint,2,opt,name=page_size,json=pageSize,proto3,oneof" json:"page_size,omitempty"`
	// 与过滤参数
	Query *string `protobuf:"bytes,3,opt,name=query,proto3,oneof" json:"query,omitempty"`
	// 或过滤参数
	OrQuery *string `protobuf:"bytes,4,opt,name=or_query,json=or,proto3,oneof" json:"or_query,omitempty"`
	// 排序条件
	OrderBy []string `protobuf:"bytes,5,rep,name=order_by,json=orderBy,proto3" json:"order_by,omitempty"`
	// 是否不分页
	NoPaging *bool `protobuf:"varint,6,opt,name=no_paging,json=nopaging,proto3,oneof" json:"no_paging,omitempty"`
	// 字段掩码
	FieldMask *fieldmaskpb.FieldMask `protobuf:"bytes,7,opt,name=field_mask,json=fieldMask,proto3" json:"field_mask,omitempty"`
}

// 创建用户 - 请求
type CreateUserRequest struct {
	User       *User  `protobuf:"bytes,1,opt,name=user,proto3" json:"user,omitempty"`
	OperatorId uint32 `protobuf:"varint,2,opt,name=operatorId,proto3" json:"operatorId,omitempty"`
}

// 更新用户 - 请求
type UpdateUserRequest struct {
	Id         uint32 `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	User       *User  `protobuf:"bytes,2,opt,name=user,proto3" json:"user,omitempty"`
	OperatorId uint32 `protobuf:"varint,3,opt,name=operatorId,proto3" json:"operatorId,omitempty"`
}

// 删除用户 - 请求
type DeleteUserRequest struct {
	Id         uint32 `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	OperatorId uint32 `protobuf:"varint,2,opt,name=operatorId,proto3" json:"operatorId,omitempty"`
}

// 验证密码 - 请求
type VerifyPasswordRequest struct {
	UserName string `protobuf:"bytes,1,opt,name=userName,proto3" json:"userName,omitempty"`
	Password string `protobuf:"bytes,2,opt,name=password,proto3" json:"password,omitempty"`
}

// 验证密码 - 答复
type VerifyPasswordResponse struct {
	Result VerifyPasswordResult `protobuf:"varint,1,opt,name=result,proto3,enum=user.service.v1.VerifyPasswordResult" json:"result,omitempty"`
}

// 用户是否存在 - 请求
type UserExistsRequest struct {
	UserName string `protobuf:"bytes,1,opt,name=userName,proto3" json:"userName,omitempty"`
}

// 用户是否存在 - 答复
type UserExistsResponse struct {
	Exist bool `protobuf:"varint,1,opt,name=exist,proto3" json:"exist,omitempty"`
}

// This is a compile-time assertion to ensure that this generated file
// is compatible with the kratos package it is being compiled against.
var _ = new(context.Context)
var _ = binding.EncodeURL

const _ = http.SupportPackageIsVersion1

const OperationUserServiceCreateUser = "/admin.service.v1.UserService/CreateUser"
const OperationUserServiceDeleteUser = "/admin.service.v1.UserService/DeleteUser"
const OperationUserServiceGetUser = "/admin.service.v1.UserService/GetUser"
const OperationUserServiceListUser = "/admin.service.v1.UserService/ListUser"
const OperationUserServiceUpdateUser = "/admin.service.v1.UserService/UpdateUser"

type UserServiceHTTPServer interface {
	// CreateUser 创建用户
	CreateUser(context.Context, *CreateUserRequest) (*User, error)
	// DeleteUser 删除用户
	DeleteUser(context.Context, *DeleteUserRequest) (*emptypb.Empty, error)
	// GetUser 获取用户数据
	GetUser(context.Context, *GetUserRequest) (*User, error)
	// ListUser 获取用户列表
	ListUser(context.Context, *PagingRequest) (*ListUserResponse, error)
	// UpdateUser 更新用户
	UpdateUser(context.Context, *UpdateUserRequest) (*User, error)
}

func RegisterUserServiceHTTPServer(s *Server, srv UserServiceHTTPServer) {
	r := s.Route("/")
	r.GET("/admin/v1/users", _UserService_ListUser0_HTTP_Handler(srv))
	r.GET("/admin/v1/users/{id}", _UserService_GetUser0_HTTP_Handler(srv))
	r.POST("/admin/v1/users", _UserService_CreateUser0_HTTP_Handler(srv))
	r.PUT("/admin/v1/users/{id}", _UserService_UpdateUser0_HTTP_Handler(srv))
	r.DELETE("/admin/v1/users/{id}", _UserService_DeleteUser0_HTTP_Handler(srv))
}

func _UserService_ListUser0_HTTP_Handler(srv UserServiceHTTPServer) func(ctx Context) error {
	return func(ctx Context) error {
		var in PagingRequest
		if err := ctx.BindQuery(&in); err != nil {
			return err
		}
		SetOperation(ctx, OperationUserServiceListUser)
		h := ctx.Middleware(func(ctx context.Context, req interface{}) (interface{}, error) {
			return srv.ListUser(ctx, req.(*PagingRequest))
		})
		out, err := h(ctx, &in)
		if err != nil {
			return err
		}
		reply := out.(*ListUserResponse)
		return ctx.Result(200, reply)
	}
}

func _UserService_GetUser0_HTTP_Handler(srv UserServiceHTTPServer) func(ctx Context) error {
	return func(ctx Context) error {
		var in GetUserRequest
		if err := ctx.BindQuery(&in); err != nil {
			return err
		}
		if err := ctx.BindVars(&in); err != nil {
			return err
		}
		SetOperation(ctx, OperationUserServiceGetUser)
		h := ctx.Middleware(func(ctx context.Context, req interface{}) (interface{}, error) {
			return srv.GetUser(ctx, req.(*GetUserRequest))
		})
		out, err := h(ctx, &in)
		if err != nil {
			return err
		}
		reply := out.(*User)
		return ctx.Result(200, reply)
	}
}

func _UserService_CreateUser0_HTTP_Handler(srv UserServiceHTTPServer) func(ctx Context) error {
	return func(ctx Context) error {
		var in CreateUserRequest
		if err := ctx.Bind(&in.User); err != nil {
			return err
		}
		if err := ctx.BindQuery(&in); err != nil {
			return err
		}
		SetOperation(ctx, OperationUserServiceCreateUser)
		h := ctx.Middleware(func(ctx context.Context, req interface{}) (interface{}, error) {
			return srv.CreateUser(ctx, req.(*CreateUserRequest))
		})
		out, err := h(ctx, &in)
		if err != nil {
			return err
		}
		reply := out.(*User)
		return ctx.Result(200, reply)
	}
}

func _UserService_UpdateUser0_HTTP_Handler(srv UserServiceHTTPServer) func(ctx Context) error {
	return func(ctx Context) error {
		var in UpdateUserRequest
		if err := ctx.Bind(&in.User); err != nil {
			return err
		}
		if err := ctx.BindQuery(&in); err != nil {
			return err
		}
		if err := ctx.BindVars(&in); err != nil {
			return err
		}
		SetOperation(ctx, OperationUserServiceUpdateUser)
		h := ctx.Middleware(func(ctx context.Context, req interface{}) (interface{}, error) {
			return srv.UpdateUser(ctx, req.(*UpdateUserRequest))
		})
		out, err := h(ctx, &in)
		if err != nil {
			return err
		}
		reply := out.(*User)
		return ctx.Result(200, reply)
	}
}

func _UserService_DeleteUser0_HTTP_Handler(srv UserServiceHTTPServer) func(ctx Context) error {
	return func(ctx Context) error {
		var in DeleteUserRequest
		if err := ctx.BindQuery(&in); err != nil {
			return err
		}
		if err := ctx.BindVars(&in); err != nil {
			return err
		}
		SetOperation(ctx, OperationUserServiceDeleteUser)
		h := ctx.Middleware(func(ctx context.Context, req interface{}) (interface{}, error) {
			return srv.DeleteUser(ctx, req.(*DeleteUserRequest))
		})
		out, err := h(ctx, &in)
		if err != nil {
			return err
		}
		reply := out.(*emptypb.Empty)
		return ctx.Result(200, reply)
	}
}

type UserServiceHTTPClient interface {
	CreateUser(ctx context.Context, req *CreateUserRequest, opts ...http.CallOption) (rsp *User, err error)
	DeleteUser(ctx context.Context, req *DeleteUserRequest, opts ...http.CallOption) (rsp *emptypb.Empty, err error)
	GetUser(ctx context.Context, req *GetUserRequest, opts ...http.CallOption) (rsp *User, err error)
	ListUser(ctx context.Context, req *PagingRequest, opts ...http.CallOption) (rsp *ListUserResponse, err error)
	UpdateUser(ctx context.Context, req *UpdateUserRequest, opts ...http.CallOption) (rsp *User, err error)
}

type UserServiceHTTPClientImpl struct {
	cc *http.Client
}

func NewUserServiceHTTPClient(client *http.Client) UserServiceHTTPClient {
	return &UserServiceHTTPClientImpl{client}
}

func (c *UserServiceHTTPClientImpl) CreateUser(ctx context.Context, in *CreateUserRequest, opts ...http.CallOption) (*User, error) {
	var out User
	pattern := "/admin/v1/users"
	path := binding.EncodeURL(pattern, in, false)
	opts = append(opts, http.Operation(OperationUserServiceCreateUser))
	opts = append(opts, http.PathTemplate(pattern))
	err := c.cc.Invoke(ctx, "POST", path, in.User, &out, opts...)
	if err != nil {
		return nil, err
	}
	return &out, err
}

func (c *UserServiceHTTPClientImpl) DeleteUser(ctx context.Context, in *DeleteUserRequest, opts ...http.CallOption) (*emptypb.Empty, error) {
	var out emptypb.Empty
	pattern := "/admin/v1/users/{id}"
	path := binding.EncodeURL(pattern, in, true)
	opts = append(opts, http.Operation(OperationUserServiceDeleteUser))
	opts = append(opts, http.PathTemplate(pattern))
	err := c.cc.Invoke(ctx, "DELETE", path, nil, &out, opts...)
	if err != nil {
		return nil, err
	}
	return &out, err
}

func (c *UserServiceHTTPClientImpl) GetUser(ctx context.Context, in *GetUserRequest, opts ...http.CallOption) (*User, error) {
	var out User
	pattern := "/admin/v1/users/{id}"
	path := binding.EncodeURL(pattern, in, true)
	opts = append(opts, http.Operation(OperationUserServiceGetUser))
	opts = append(opts, http.PathTemplate(pattern))
	err := c.cc.Invoke(ctx, "GET", path, nil, &out, opts...)
	if err != nil {
		return nil, err
	}
	return &out, err
}

func (c *UserServiceHTTPClientImpl) ListUser(ctx context.Context, in *PagingRequest, opts ...http.CallOption) (*ListUserResponse, error) {
	var out ListUserResponse
	pattern := "/admin/v1/users"
	path := binding.EncodeURL(pattern, in, true)
	opts = append(opts, http.Operation(OperationUserServiceListUser))
	opts = append(opts, http.PathTemplate(pattern))
	err := c.cc.Invoke(ctx, "GET", path, nil, &out, opts...)
	if err != nil {
		return nil, err
	}
	return &out, err
}

func (c *UserServiceHTTPClientImpl) UpdateUser(ctx context.Context, in *UpdateUserRequest, opts ...http.CallOption) (*User, error) {
	var out User
	pattern := "/admin/v1/users/{id}"
	path := binding.EncodeURL(pattern, in, false)
	opts = append(opts, http.Operation(OperationUserServiceUpdateUser))
	opts = append(opts, http.PathTemplate(pattern))
	err := c.cc.Invoke(ctx, "PUT", path, in.User, &out, opts...)

	if errors.Is(err, context.DeadlineExceeded) {
		return nil, errors.GatewayTimeout("Timeout", err.Error())
	}

	if err != nil {
		return nil, err
	}
	return &out, err
}
