package ghttp

import (
	"bytes"
	"context"
	"crypto/tls"
	"gitee.com/weixuehao/kratos-bootloader/pkg/common/endpoint"
	"gitee.com/weixuehao/kratos-bootloader/pkg/common/host"
	"gitee.com/weixuehao/kratos-bootloader/pkg/common/matcher"
	"github.com/go-kratos/kratos/v2/middleware"
	"net"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/gin-gonic/gin"

	"github.com/go-kratos/kratos/v2/errors"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/go-kratos/kratos/v2/transport"
	kHttp "github.com/go-kratos/kratos/v2/transport/http"
)

var (
	_ transport.Server     = (*Server)(nil)
	_ transport.Endpointer = (*Server)(nil)
	_ http.Handler         = (*Server)(nil)
)

type BodyWriter struct {
	gin.ResponseWriter
	Body *bytes.Buffer
}

func (w BodyWriter) Write(b []byte) (int, error) {
	w.Body.Write(b)
	return w.ResponseWriter.Write(b)
}

type Server struct {
	*gin.Engine
	*http.Server
	ginOpt   []gin.OptionFunc
	endpoint *url.URL
	tlsConf  *tls.Config
	timeout  time.Duration
	lis      net.Listener
	network  string
	address  string

	err error

	filters    []FilterFunc
	middleware matcher.Matcher
	decVars    kHttp.DecodeRequestFunc
	decQuery   kHttp.DecodeRequestFunc
	decBody    kHttp.DecodeRequestFunc
	enc        kHttp.EncodeResponseFunc
	ene        kHttp.EncodeErrorFunc
}

func NewServer(opts ...ServerOption) *Server {
	srv := &Server{
		Server:     &http.Server{},
		ginOpt:     []gin.OptionFunc{},
		network:    "tcp",
		timeout:    1 * time.Second,
		middleware: matcher.New(),
		decVars:    DefaultRequestVars,
		decQuery:   DefaultRequestQuery,
		decBody:    DefaultRequestDecoder,
		enc:        DefaultResponseEncoder,
		ene:        DefaultErrorEncoder,
	}

	if port := os.Getenv("PORT"); port != "" {
		log.Infof("Environment variable PORT=\"%s\"", port)
		srv.address = ":" + port
	}
	log.Info("Environment variable PORT is undefined. Using port :8080 by default")
	srv.address = ":8080"

	srv.init(opts...)

	return srv
}

func (s *Server) init(opts ...ServerOption) {
	for _, o := range opts {
		o(s)
	}
	s.Engine = gin.Default(s.ginOpt...)
	s.Engine.Use(s.filter())
	s.Server.Addr = s.address
	s.Server.Handler = FilterChain(s.filters...)(s.Engine.Handler())
	s.Server.TLSConfig = s.tlsConf
}

// Use uses a service middleware with selector.
// selector:
//   - '/*'
//   - '/helloWorld.v1.Greeter/*'
//   - '/helloWorld.v1.Greeter/SayHello'
func (s *Server) Use(selector string, m ...middleware.Middleware) {
	s.middleware.Add(selector, m...)
}

// Route registers an HTTP router.
func (s *Server) Route(prefix string, filters ...FilterFunc) *Router {
	return newRouter(prefix, s, filters...)
}

// HandlePrefix registers a new route with a matcher for the URL path prefix.
func (s *Server) HandlePrefix(prefix string, h http.Handler) {
	s.Engine.Group(prefix, GinHandler(h))
}

// HandleFunc registers a new route with a matcher for the URL path.
func (s *Server) HandleFunc(path string, h http.HandlerFunc) {
	s.Engine.Group(path, GinHandlerFunc(h))
}

// ServeHTTP should write reply headers and data to the ResponseWriter and then return.
func (s *Server) ServeHTTP(res http.ResponseWriter, req *http.Request) {
	s.Engine.ServeHTTP(res, req)
}

func (s *Server) filter() gin.HandlerFunc {
	return func(c *gin.Context) {
		var (
			ctx    context.Context
			cancel context.CancelFunc
		)
		if s.timeout > 0 {
			ctx, cancel = context.WithTimeout(c.Request.Context(), s.timeout)
		} else {
			ctx, cancel = context.WithCancel(c.Request.Context())
		}
		defer cancel()

		ctx = NewGinContext(ctx, c)

		pathTemplate := c.Request.URL.Path
		if route := c.FullPath(); route != "" {
			// /path/123 -> /user/:id
			pathTemplate = c.FullPath()
		}

		tr := &Transport{
			operation:    pathTemplate,
			pathTemplate: pathTemplate,
			reqHeader:    headerCarrier(c.Request.Header),
			replyHeader:  headerCarrier(c.Writer.Header()),
			request:      c.Request,
			response:     c.Writer,
		}
		if s.endpoint != nil {
			tr.endpoint = s.endpoint.String()
		}

		tr.request = c.Request.WithContext(transport.NewServerContext(ctx, tr))
		c.Request = tr.request

		c.Next()
	}
}

// Endpoint return a real address to registry endpoint.
// examples:
//
//	https://127.0.0.1:8000
//	Legacy: http://127.0.0.1:8000?isSecure=false
func (s *Server) Endpoint() (*url.URL, error) {
	if err := s.listenAndEndpoint(); err != nil {
		return nil, err
	}
	return s.endpoint, nil
}

// Start the HTTP server.
func (s *Server) Start(ctx context.Context) error {
	if err := s.listenAndEndpoint(); err != nil {
		return err
	}
	s.BaseContext = func(net.Listener) context.Context {
		return ctx
	}
	log.Infof("[GIN] server listening on: %s", s.lis.Addr().String())
	var err error
	if s.tlsConf != nil {
		err = s.ServeTLS(s.lis, "", "")
	} else {
		err = s.Serve(s.lis)
	}
	if !errors.Is(err, http.ErrServerClosed) {
		return err
	}
	return nil
}

// Stop the HTTP server.
func (s *Server) Stop(ctx context.Context) error {
	log.Info("[GIN] server stopping")
	return s.Shutdown(ctx)
}

func (s *Server) listenAndEndpoint() error {
	if s.lis == nil {
		lis, err := net.Listen(s.network, s.address)
		if err != nil {
			s.err = err
			return err
		}
		s.lis = lis
	}
	if s.endpoint == nil {
		addr, err := host.Extract(s.address, s.lis)
		if err != nil {
			s.err = err
			return err
		}
		s.endpoint = endpoint.NewEndpoint(endpoint.Scheme("http", s.tlsConf != nil), addr)
	}
	return s.err
}

type ginKey struct{}

// NewGinContext returns a new Context that carries gin.Context value.
func NewGinContext(ctx context.Context, c *gin.Context) context.Context {
	return context.WithValue(ctx, ginKey{}, c)
}

// FromGinContext returns the gin.Context value stored in ctx, if any.
func FromGinContext(ctx context.Context) (c *gin.Context, ok bool) {
	c, ok = ctx.Value(ginKey{}).(*gin.Context)
	return
}
