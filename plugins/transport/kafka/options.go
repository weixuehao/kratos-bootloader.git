package kafka

import (
	"gitee.com/weixuehao/kratos-bootloader/plugins/broker"
	"github.com/go-kratos/kratos/v2/log"
	"go.opentelemetry.io/otel/propagation"
	"go.opentelemetry.io/otel/trace"
)

type ServerOption func(o *Server)

// WithBrokerOptions MQ代理配置
func WithBrokerOptions(opts ...broker.Option) ServerOption {
	return func(s *Server) {
		s.brokerOpts = append(s.brokerOpts, opts...)
	}
}

func WithLogger(logger *log.Helper) ServerOption {
	return func(e *Server) { e.log = logger }
}

// WithAddress MQ代理地址
func WithAddress(addrs []string) ServerOption {
	return func(s *Server) {
		s.brokerOpts = append(s.brokerOpts, broker.WithAddress(addrs...))
	}
}

//// WithTLSConfig TLS配置
//func WithTLSConfig(c *tls.Config) ServerOption {
//	return func(s *Server) {
//		if c != nil {
//			s.brokerOpts = append(s.brokerOpts, broker.WithEnableSecure(true))
//		}
//		s.brokerOpts = append(s.brokerOpts, broker.WithTLSConfig(c))
//	}
//}

//// WithCodec 编解码器
//func WithCodec(c string) ServerOption {
//	return func(s *Server) {
//		s.brokerOpts = append(s.brokerOpts, broker.WithCodec(c))
//	}
//}

// WithEnableKeepAlive enable keep alive
func WithEnableKeepAlive(enable bool) ServerOption {
	return func(s *Server) {
		s.enableKeepAlive = enable
	}
}

// WithTracerProvider 注入链路追踪器的Provider
func WithTracerProvider(provider trace.TracerProvider, tracerName string) ServerOption {
	return func(s *Server) {
		s.brokerOpts = append(s.brokerOpts, broker.WithTracer(broker.WithTracerProvider(provider),
			broker.WithTracerName(tracerName)))
	}
}

// WithPropagator 注入链路追踪器的Propagator
func WithPropagator(propagators propagation.TextMapPropagator) ServerOption {
	return func(s *Server) {
		s.brokerOpts = append(s.brokerOpts, broker.WithTracer(broker.WithPropagator(propagators)))
	}
}
