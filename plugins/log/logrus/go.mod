module gitee.com/weixuehao/kratos-bootloader/plugins/log/logrus

go 1.21

toolchain go1.22.1

require (
	github.com/go-kratos/kratos/v2 v2.8.2
	github.com/sirupsen/logrus v1.9.3
)

require golang.org/x/sys v0.22.0 // indirect
