package redis

import (
	"fmt"
	"net/url"
	"testing"
)

func TestUrl(t *testing.T) {
	parse, err := url.Parse("http://:9090/hello")
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(parse.Scheme + "=" + parse.Host)
}
