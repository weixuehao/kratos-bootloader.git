module gitee.com/weixuehao/kratos-bootloader/plugins/registry/redis

go 1.21

toolchain go1.22.6

require github.com/redis/go-redis/v9 v9.6.1 // indi

require github.com/go-kratos/kratos/v2 v2.8.2

require (
	github.com/cespare/xxhash/v2 v2.3.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
)
