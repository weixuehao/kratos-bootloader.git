package redis

import (
	"context"
	"fmt"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/go-kratos/kratos/v2/registry"
	"strconv"
	"strings"
	"time"
)

var (
	_ registry.Registrar = (*Registry)(nil)
	_ registry.Discovery = (*Registry)(nil)
)

type subscriber struct {
	appName  string
	callBack func()
}

type Option func(o *Registry)

// WithContext with registry context.
func WithContext(ctx context.Context) Option {
	return func(o *Registry) { o.ctx = ctx }
}

func WithHeartbeat(interval time.Duration) Option {
	return func(o *Registry) { o.heartbeatInterval = interval }
}

func WithRefresh(interval time.Duration) Option {
	return func(o *Registry) { o.refreshInterval = interval }
}

func WithNamespace(namespace string) Option {
	return func(o *Registry) { o.namespace = namespace }
}

func WithMaxRetry(maxRetry int) Option {
	return func(e *Registry) { e.maxRetry = maxRetry }
}

func WithLogger(logger *log.Helper) Option {
	return func(e *Registry) { e.logger = logger }
}

type Registry struct {
	ctx               context.Context
	redisClient       *RedisClient
	heartbeatInterval time.Duration
	refreshInterval   time.Duration
	maxRetry          int
	namespace         string
	logger            *log.Helper
}

func New(url string, opts ...Option) (*Registry, error) {
	r := &Registry{
		ctx:               context.Background(),
		heartbeatInterval: heartbeatTime,
		refreshInterval:   refreshTime,
		maxRetry:          heartbeatRetry,
		namespace:         "default",
	}

	for _, o := range opts {
		o(r)
	}
	if r.logger == nil {
		r.logger = log.NewHelper(log.GetLogger())
	}
	client, err := NewClient(url,
		withHeartbeatInterval(r.heartbeatInterval),
		withClientContext(r.ctx),
		withNamespace(r.namespace),
		withMaxRetry(r.maxRetry),
		withLogger(r.logger),
		withRefresh(r.refreshInterval),
	)
	if err != nil {
		return nil, err
	}
	r.redisClient = client

	return r, nil

}

// Register 这里的Context是每个注册器独享的
func (r *Registry) Register(ctx context.Context, service *registry.ServiceInstance) error {
	registryIns := parseInstance(service)
	//appName := r.redisClient.toAppID(service.Name)
	//upInstances := make(map[string]struct{})
	//
	//instances, err := r.redisClient.GetService(ctx, appName)
	//if err != nil {
	//	r.redisClient.logger.Warn("redis register err: ", err)
	//}
	//for _, ins := range instances {
	//	upInstances[ins.AppID] = struct{}{}
	//}

	for _, ri := range registryIns {
		//if _, ok := upInstances[ri.AppID]; !ok {
		ri.CreateTime = time.Now().Unix()
		go r.redisClient.Heartbeat(ctx, ri)
		if err := r.redisClient.Register(ctx, ri); err != nil {
			return err
		}
		//}
	}
	return nil
}

// Deregister registry service to redis.
func (r *Registry) Deregister(ctx context.Context, service *registry.ServiceInstance) error {
	registryIns := parseInstance(service)
	for _, ri := range registryIns {
		if err := r.redisClient.Deregister(ctx, ri.AppName, ri.AppID); err != nil {
			return err
		}
	}
	return nil
}

// GetService get services from redis
func (r *Registry) GetService(ctx context.Context, serviceName string) ([]*registry.ServiceInstance, error) {
	instances, err := r.redisClient.GetService(ctx, serviceName)
	if err != nil {
		return nil, err
	}
	return parseSrvIns(instances), nil
}

// Watch 是独立的ctx
func (r *Registry) Watch(ctx context.Context, serviceName string) (registry.Watcher, error) {
	return newWatch(ctx, r.redisClient, serviceName)
}

func parseSrvIns(instances []Instance) []*registry.ServiceInstance {
	items := make([]*registry.ServiceInstance, 0, len(instances))
	for _, instance := range instances {
		if instance.Metadata["Endpoints"] != "" {
			srvIns := getServiceInstance(instance)
			srvIns.Endpoints = []string{instance.Metadata["Endpoints"]}
			srvIns.Metadata["AddrS"] = instance.Metadata["Endpoints"]
			items = append(items, srvIns)
		} else if instance.Metadata["Endpoints"] == "" && instance.AddrS != nil && len(instance.AddrS) > 0 {
			for _, addr := range instance.AddrS {
				srvIns := getServiceInstance(instance)
				srvIns.Endpoints = []string{addr}
				srvIns.Metadata["AddrS"] = addr
				items = append(items, srvIns)
			}
		}
	}
	return items
}

func getServiceInstance(ins Instance) *registry.ServiceInstance {
	srvIns := &registry.ServiceInstance{
		ID:      ins.Metadata["ID"],
		Name:    ins.Metadata["Name"],
		Version: ins.Metadata["Version"],
		//Endpoints: []string{instance.Metadata["Endpoints"]},
		Metadata: ins.Metadata,
	}
	srvIns.Metadata["AppName"] = ins.AppName
	srvIns.Metadata["AppID"] = ins.AppID
	srvIns.Metadata["CreateTime"] = strconv.FormatInt(ins.CreateTime, 10)
	srvIns.Metadata["LastHeartBeatTIme"] = strconv.FormatInt(ins.LastHeartBeatTIme, 10)
	srvIns.Metadata["Zone"] = ins.Zone
	srvIns.Metadata["Env"] = ins.Env
	srvIns.Metadata["Tags"] = ins.Tags
	srvIns.Metadata["HostName"] = ins.HostName
	//srvIns.Metadata["AddrS"] = strings.Join(instance.AddrS,",")
	srvIns.Metadata["Version"] = ins.Version
	srvIns.Metadata["Tenant"] = ins.Tenant
	srvIns.Metadata["Weight"] = strconv.Itoa(ins.Weight)
	srvIns.Metadata["CurrentLoad"] = strconv.FormatUint(ins.CurrentLoad, 10)
	srvIns.Metadata["MaxLoad"] = strconv.FormatUint(ins.MaxLoad, 10)
	srvIns.Metadata["DataSubscribe"] = intSliceToString(ins.DataSubscribe)
	return srvIns
}

func parseInstance(service *registry.ServiceInstance) []Instance {
	res := make([]Instance, 0, len(service.Endpoints))

	endpoints := service.Endpoints
	if service.Metadata["AddrS"] != "" {
		endpoints = strings.Split(service.Metadata["AddrS"], ",")
	}
	for _, ep := range endpoints {
		//start := strings.Index(ep, "//")
		end := strings.LastIndex(ep, ":")
		//ip := ep[start+2 : end]
		sport := ep[end+1:]
		hostName := service.ID
		appName := strings.ToUpper(service.Name)
		metadata := make(map[string]string)
		if len(service.Metadata) > 0 {
			metadata = service.Metadata
		}
		if s, ok := service.Metadata["hostName"]; ok {
			hostName = s
		}
		metadata["ID"] = service.ID
		metadata["Name"] = service.Name
		metadata["Version"] = service.Version
		metadata["Endpoints"] = ep
		metadata["agent"] = "go-redis-client"
		ins := Instance{
			AppName:           appName,
			CreateTime:        time.Now().Unix(),
			LastHeartBeatTIme: time.Now().Unix(),
			Zone:              metadata["Zone"],
			Env:               metadata["Env"],
			Tags:              metadata["Tags"],
			AddrS:             []string{ep},
			HostName:          hostName,
			Version:           service.Version,
			Tenant:            metadata["Tenant"],
			Metadata:          metadata,
		}
		ins.CreateTime, _ = strconv.ParseInt(metadata["CreateTime"], 10, 64)
		ins.LastHeartBeatTIme, _ = strconv.ParseInt(metadata["LastHeartBeatTIme"], 10, 64)
		ins.Weight, _ = strconv.Atoi(metadata["Weight"])
		ins.CurrentLoad, _ = strconv.ParseUint(metadata["CurrentLoad"], 10, 64)
		ins.MaxLoad, _ = strconv.ParseUint(metadata["MaxLoad"], 10, 64)
		ins.DataSubscribe, _ = stringToIntSlice(metadata["DataSubscribe"])
		//instanceID := strings.Join([]string{ip, appName, sport}, ":")
		ver := strings.Replace(service.Version, "'", "", 2)
		instanceID := fmt.Sprintf(
			"%s:%s:%s:%d:%s", ins.Tenant, ins.HostName, sport, ins.Weight, ver)
		ins.AppID = instanceID

		res = append(res, ins)
	}
	return res
}

func intSliceToString(ints []int) string {
	// 创建一个字符串切片来存储转换后的字符串
	strSlice := make([]string, len(ints))
	for i, v := range ints {
		strSlice[i] = strconv.Itoa(v) // 将整数转为字符串
	}
	return strings.Join(strSlice, ",") // 用逗号连接
}

func stringToIntSlice(s string) ([]int, error) {
	// 按逗号分割字符串
	strSlice := strings.Split(s, ",")
	ints := make([]int, len(strSlice))

	for i, v := range strSlice {
		intValue, err := strconv.Atoi(v) // 转换为整数
		if err != nil {
			return nil, err // 处理错误
		}
		ints[i] = intValue
	}
	return ints, nil
}
