package redis

import (
	"context"

	"github.com/go-kratos/kratos/v2/registry"
)

var _ registry.Watcher = (*watcher)(nil)

type watcher struct {
	ctx        context.Context
	cancel     context.CancelFunc
	cli        *RedisClient
	watchChan  chan struct{}
	serverName string
}

func newWatch(ctx context.Context, cli *RedisClient, serverName string) (*watcher, error) {
	w := &watcher{
		ctx:        ctx,
		cli:        cli,
		serverName: serverName,
		watchChan:  make(chan struct{}, 1),
	}
	w.ctx, w.cancel = context.WithCancel(ctx)
	e := w.cli.Subscribe(
		serverName,
		func() {
			w.watchChan <- struct{}{}
		},
	)
	return w, e
}

func (w *watcher) Next() ([]*registry.ServiceInstance, error) {
	select {
	case <-w.ctx.Done():
		return nil, w.ctx.Err()
	case <-w.watchChan:
		instances, err := w.cli.GetService(w.ctx, w.serverName)
		if err != nil {
			return nil, err
		}
		services := parseSrvIns(instances)
		return services, err
	}
}

func (w *watcher) Stop() error {
	w.cancel()
	w.cli.Unsubscribe(w.serverName)
	return nil
}
