package redis

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/redis/go-redis/v9"
	"net"
	"net/url"
	"strconv"
	"strings"
	"sync"
	"time"
)

var _ redis.Hook = (*RedisClient)(nil)

const (
	statusUp       = "UP"
	statusDown     = "DOWN"
	DiscoverPrefix = "Discovery" // 注册模板
	ExtendPrefix   = "Ex"        // 扩展数据
	heartbeatRetry = 3
	maxIdleConns   = 100
	heartbeatTime  = 10 * time.Second
	refreshTime    = 30 * time.Second
)

type Instance struct {
	AppName           string            `json:"AppName"`                 // 应用名称, 所属应用集群
	AppID             string            `json:"AppID"`                   // 实例ID， 不能重复
	CreateTime        int64             `json:"CreateTime"`              // 记录创建时间
	LastHeartBeatTIme int64             `json:"LastHeartBeatTIme"`       // 心跳时间
	Zone              string            `json:"Zone"`                    // 机房
	Env               string            `json:"Env"`                     // 环境   Dev ; Fat ; Uat ; Pro
	Tags              string            `json:"Tags"`                    // 标签，格式：**,**,**  例如： "grayscale,newRedis,noProxy"
	HostName          string            `json:"HostName"`                // 主机名
	AddrS             []string          `json:"AddrS"`                   // 地址列表
	Version           string            `json:"Version"`                 // 版本号
	Tenant            string            `json:"Tenant"`                  // 租户ID
	Weight            int               `json:"Weight"`                  // 权重，程序默认
	CurrentLoad       uint64            `json:"TaskCount"`               // 当前负载数量
	MaxLoad           uint64            `json:"MAXTaskCount"`            // 最大负载数量 并发问题,无锁或读锁时使用atomic
	Metadata          map[string]string `json:"Metadata"`                // 元数据
	DataSubscribe     []int             `json:"DataSubscribe,omitempty"` // 数据中心数据订阅
}

type Port struct {
	Port    int    `json:"$"`
	Enabled string `json:"@enabled"`
}

type DataCenterInfo struct {
	Name  string `json:"name"`
	Class string `json:"@class"`
}

type ClientOption func(e *RedisClient)

func withMaxRetry(maxRetry int) ClientOption {
	return func(e *RedisClient) { e.maxRetry = maxRetry }
}

func withHeartbeatInterval(interval time.Duration) ClientOption {
	return func(e *RedisClient) {
		e.heartbeatInterval = interval
	}
}

func withClientContext(ctx context.Context) ClientOption {
	return func(e *RedisClient) { e.ctx = ctx }
}

func withNamespace(namespace string) ClientOption {
	return func(e *RedisClient) { e.namespace = namespace }
}

func withRefresh(interval time.Duration) ClientOption {
	return func(o *RedisClient) { o.refreshInterval = interval }
}

func withLogger(logger *log.Helper) ClientOption {
	return func(e *RedisClient) { e.logger = logger }
}

type RedisClient struct {
	ctx          context.Context
	addr         string
	password     string
	dB           int
	maxRetries   int
	poolSize     int
	readTimeout  time.Duration
	writeTimeout time.Duration
	connOk       bool
	readOnly     bool

	client            *redis.Client
	namespace         string
	maxRetry          int
	heartbeatInterval time.Duration
	refreshInterval   time.Duration
	keepalive         map[string]chan struct{}
	discoveryApps     map[string]struct{}
	allInstances      map[string][]Instance
	subscribers       map[string]*subscriber

	lock   sync.Mutex
	logger *log.Helper
}

func NewClient(_url string, opts ...ClientOption) (*RedisClient, error) {
	r := &RedisClient{
		ctx:               context.Background(),
		namespace:         "default",
		maxRetry:          heartbeatRetry,
		heartbeatInterval: heartbeatTime,
		refreshInterval:   refreshTime,
		keepalive:         make(map[string]chan struct{}),
		discoveryApps:     make(map[string]struct{}),
		allInstances:      make(map[string][]Instance),
		subscribers:       make(map[string]*subscriber),
	}
	if err := r.daliWithURL(_url); err != nil {
		return nil, err
	}
	for _, o := range opts {
		o(r)
	}
	if r.logger == nil {
		r.logger = log.NewHelper(log.GetLogger())
	}
	// it is required to broadcast for the first time
	go r.broadcast()
	go r.refresh(r.ctx)
	return r, nil
}

func (r *RedisClient) FetchAppInstances(ctx context.Context, appName string) ([]Instance, error) {
	var res []Instance
	if srvs, err := r.client.Keys(ctx, fmt.Sprintf("%s.%s.%s.*", DiscoverPrefix, r.namespace, appName)).Result(); err != nil {
		return nil, err
	} else {
		for _, key := range srvs {
			var ins Instance
			// 获取基础配置
			if value, errR := r.client.Get(ctx, key).Result(); errR == nil {
				if errUnM := json.Unmarshal([]byte(value), &ins); errUnM == nil {
					res = append(res, ins)
				} else {
					return nil, errUnM
				}
			} else {
				return nil, errR
			}
		}

	}
	r.lock.Lock()
	r.discoveryApps[appName] = struct{}{}
	r.lock.Unlock()
	return res, nil
}

func (r *RedisClient) GetService(ctx context.Context, serverName string) ([]Instance, error) {
	appName := r.toAppID(serverName)
	if ins, ok := r.allInstances[appName]; ok {
		return ins, nil
	}
	// if not in allInstances of API, you can try to obtain it separately again
	return r.FetchAppInstances(ctx, appName)
}

func (r *RedisClient) Down(ctx context.Context, appName, instanceID string) error {
	prefix := r.keyPrefix(appName, instanceID)
	if _, err := r.client.Del(ctx, prefix).Result(); err != nil {
		return err
	}
	return nil
}

func (r *RedisClient) Deregister(ctx context.Context, appName, instanceID string) error {
	if err := r.Down(ctx, appName, instanceID); err != nil {
		return err
	}
	go r.cancelHeartbeat(appName)
	return nil
}

// Register 内部调用函数
func (r *RedisClient) Register(ctx context.Context, ins Instance) error {
	data, err := json.Marshal(ins)
	if err != nil {
		return err
	}
	prefix := r.keyPrefix(ins.AppName, ins.AppID)
	if _, err := r.client.Set(ctx, prefix, data, (r.heartbeatInterval+5)*time.Second).Result(); err != nil {
		return err
	} else {
		return nil
	}
}

func (r *RedisClient) Heartbeat(ctx context.Context, ins Instance) {
	r.lock.Lock()
	r.keepalive[ins.AppName] = make(chan struct{})
	r.lock.Unlock()

	ticker := time.NewTicker(r.heartbeatInterval)
	defer ticker.Stop()
	retryCount := 0
	for {
		select {
		case <-ctx.Done():
			return
		case <-r.keepalive[ins.AppName]:
			return
		case <-ticker.C:
			prefix := r.keyPrefix(ins.AppName, ins.AppID)
			if b, err := r.client.Expire(ctx, prefix, (r.heartbeatInterval+5)*time.Second).Result(); !b || err != nil {
				log.Errorf("redis hearBeat fail err: %s ", err)
				if retryCount++; retryCount > r.maxRetry {
					ins.LastHeartBeatTIme = time.Now().Unix()
					if err = r.Register(ctx, ins); err != nil {
						log.Errorf("eureka register fail err: %s ", err)
					}
					retryCount = 0
				}
			}
		}
	}
}

func (r *RedisClient) cancelHeartbeat(appName string) {
	r.lock.Lock()
	defer r.lock.Unlock()
	if ch, ok := r.keepalive[appName]; ok {
		ch <- struct{}{}
	}
	delete(r.discoveryApps, appName)
}

func (e *RedisClient) refresh(ctx context.Context) {
	ticker := time.NewTicker(e.refreshInterval)
	defer ticker.Stop()
	for {
		select {
		case <-ctx.Done():
			return
		case <-ticker.C:
			e.broadcast()
		}
	}
}

func (r *RedisClient) broadcast() {
	instances := r.cacheAllInstances()
	if instances == nil {
		return
	}

	r.lock.Lock()
	r.allInstances = instances
	r.lock.Unlock()

	for _, subscriber := range r.subscribers {
		go subscriber.callBack()
	}
}

func (r *RedisClient) cacheAllInstances() map[string][]Instance {
	items := make(map[string][]Instance)
	for appName := range r.discoveryApps {
		instances, _ := r.FetchAppInstances(context.Background(), appName)
		for _, instance := range instances {
			items[instance.AppName] = append(items[instance.AppName], instance)
		}
	}
	return items
}

func (r *RedisClient) Subscribe(serverName string, fn func()) error {
	r.lock.Lock()
	appName := r.toAppID(serverName)
	r.subscribers[appName] = &subscriber{
		appName:  appName,
		callBack: fn,
	}
	r.lock.Unlock()
	go r.broadcast()
	return nil
}

func (r *RedisClient) Unsubscribe(serverName string) {
	r.lock.Lock()
	delete(r.subscribers, r.toAppID(serverName))
	delete(r.discoveryApps, r.toAppID(serverName))
	r.lock.Unlock()
}

func (r *RedisClient) toAppID(serverName string) string {
	return strings.ToUpper(serverName)
}

func (r *RedisClient) keyPrefix(appName, instanceID string) string {
	return fmt.Sprintf("%s.%s.%s.%s", DiscoverPrefix, r.namespace, appName, instanceID)
}

func (r *RedisClient) getExKey(appName, instanceID string) string { //nolint:unused
	return fmt.Sprintf("%s.%s.%s.%s", ExtendPrefix, r.namespace, appName, instanceID)
}

func (r *RedisClient) daliWithURL(_url string) error {
	if pUrl, err := url.Parse(_url); err != nil {
		return err
	} else {
		if args, err := url.ParseQuery(pUrl.RawQuery); err == nil { // 解析url对象
			if maxRetry, ok := args["MaxRetries"]; ok {
				mR, _ := strconv.Atoi(maxRetry[0])
				r.maxRetries = mR
			}
			if PoolSize, ok := args["PoolSize"]; ok {
				pS, _ := strconv.Atoi(PoolSize[0])
				r.poolSize = pS
			}
			if ReadTimeout, ok := args["ReadTimeout"]; ok {
				rT, _ := strconv.Atoi(ReadTimeout[0])
				r.readTimeout = time.Duration(rT) * time.Second
				r.writeTimeout = time.Duration(rT) * time.Second
			} else {
				r.readTimeout = 5 * time.Second
				r.writeTimeout = 5 * time.Second
			}
		}
		if len(pUrl.Host) == 3 {
			return errors.New("Can't Parse HostPort Config. ")
		} else {
			r.addr = pUrl.Host
			if db := strings.Split(pUrl.Path, "/"); len(db) < 2 {
				return errors.New("Can't Parse DB Config. ")
			} else {
				r.dB, _ = strconv.Atoi(db[1])
			}
			r.password, _ = pUrl.User.Password()
		}
	}
	if err := r.initConn(); err != nil {
		return err
	} else {
		r.connOk = true
		go r.goKeepAlive() // 发送心跳保活
		return nil
	}
}

func (r *RedisClient) initConn() error {
	if r.client != nil {
		if err := r.client.Close(); err != nil {
			return fmt.Errorf("close redis client fail: %s", err)
		}
	}
	client := redis.NewClient(&redis.Options{
		Addr:            r.addr,       //
		Password:        r.password,   // no password set
		DB:              r.dB,         // use default DB
		MaxRetries:      r.maxRetries, //
		PoolSize:        r.poolSize,   //
		DialTimeout:     10 * time.Second,
		PoolTimeout:     1 * time.Second,
		ConnMaxIdleTime: 20 * time.Second,
		ReadTimeout:     r.readTimeout,
		WriteTimeout:    r.writeTimeout,
	})
	r.client = client
	r.client.AddHook(r)
	if _, err := r.client.Ping(r.ctx).Result(); err != nil {
		return err
	} else {
		return nil
	}
}

func (r *RedisClient) connWasOk() bool {
	return r.connOk
}

func (r *RedisClient) goKeepAlive() {
	for {
		if _, err := r.client.Ping(r.ctx).Result(); err != nil {
			r.connOk = false
			r.logger.Error("Connection Loss, ReBuilding... ")
			if err2 := r.initConn(); err2 != nil {
				r.logger.Error("Connection ReBuilding Failed: ", err2)
			} else {
				r.connOk = true
				r.logger.Info("Connection OK, ReBuilding ok ")
			}
			time.Sleep(3 * time.Second)
			continue
		} else {
			r.connOk = true
		}
		time.Sleep(time.Second * 10)
	}
}

// DialHook 实现 DialHook 方法, 在创建网络连接时调用。
func (h *RedisClient) DialHook(next redis.DialHook) redis.DialHook {
	return func(ctx context.Context, network, addr string) (conn net.Conn, err error) {
		//fmt.Println("Before dialing")
		conn, err = next(ctx, network, addr)
		//fmt.Println("After dialing")
		return conn, err
	}
}

// ProcessHook 实现 ProcessHook 方法, 在处理单个命令时调用。
func (h *RedisClient) ProcessHook(next redis.ProcessHook) redis.ProcessHook {
	return func(ctx context.Context, cmd redis.Cmder) error {
		//fmt.Println("Before processing command")
		err := next(ctx, cmd)
		//fmt.Println("After processing command")
		return err
	}
}

// ProcessPipelineHook 实现 ProcessPipelineHook 方法, 在处理命令管道时调用。
func (h *RedisClient) ProcessPipelineHook(next redis.ProcessPipelineHook) redis.ProcessPipelineHook {
	return func(ctx context.Context, cmds []redis.Cmder) error {
		//fmt.Println("Before processing pipeline")
		err := next(ctx, cmds)
		//fmt.Println("After processing pipeline")
		return err
	}
}
