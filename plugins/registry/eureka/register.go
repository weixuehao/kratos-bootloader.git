package eureka

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/go-kratos/kratos/v2/registry"
)

var (
	_ registry.Registrar = (*Registry)(nil)
	_ registry.Discovery = (*Registry)(nil)
)

type Option func(o *Registry)

// WithContext with registry context.
func WithContext(ctx context.Context) Option {
	return func(o *Registry) { o.ctx = ctx }
}

func WithHeartbeat(interval time.Duration) Option {
	return func(o *Registry) { o.heartbeatInterval = interval }
}

func WithRefresh(interval time.Duration) Option {
	return func(o *Registry) { o.refreshInterval = interval }
}

func WithEurekaPath(path string) Option {
	return func(o *Registry) { o.eurekaPath = path }
}

type Registry struct {
	ctx               context.Context
	api               *API
	heartbeatInterval time.Duration
	refreshInterval   time.Duration
	eurekaPath        string
}

func New(eurekaUrls []string, opts ...Option) (*Registry, error) {
	r := &Registry{
		ctx:               context.Background(),
		heartbeatInterval: heartbeatTime,
		refreshInterval:   refreshTime,
		eurekaPath:        "eureka",
	}

	for _, o := range opts {
		o(r)
	}

	client := NewClient(eurekaUrls,
		WithHeartbeatInterval(r.heartbeatInterval),
		WithClientContext(r.ctx),
		WithNamespace(r.eurekaPath),
	)
	r.api = NewAPI(r.ctx, client, r.refreshInterval)
	return r, nil
}

// Register 这里的Context是每个注册器独享的
func (r *Registry) Register(ctx context.Context, service *registry.ServiceInstance) error {
	return r.api.Register(ctx, service.Name, parseInstance(service)...)
}

// Deregister registry service to zookeeper.
func (r *Registry) Deregister(ctx context.Context, service *registry.ServiceInstance) error {
	return r.api.Deregister(ctx, parseInstance(service))
}

// GetService get services from zookeeper
func (r *Registry) GetService(ctx context.Context, serviceName string) ([]*registry.ServiceInstance, error) {
	instances, err := r.api.GetService(ctx, serviceName)
	if err != nil {
		return nil, err
	}
	return parseSrvIns(instances), nil
}

// Watch 是独立的ctx
func (r *Registry) Watch(ctx context.Context, serviceName string) (registry.Watcher, error) {
	return newWatch(ctx, r.api, serviceName)
}

func parseSrvIns(instances []Instance) []*registry.ServiceInstance {
	items := make([]*registry.ServiceInstance, 0, len(instances))
	for _, instance := range instances {
		srvIns := &registry.ServiceInstance{
			ID:      instance.Metadata["ID"],
			Name:    instance.Metadata["Name"],
			Version: instance.Metadata["Version"],
			//Endpoints: []string{instance.Metadata["Endpoints"]},
			Metadata: instance.Metadata,
		}
		srvIns.Metadata["instanceId"] = instance.InstanceID
		srvIns.Metadata["hostName"] = instance.HostName
		srvIns.Metadata["app"] = instance.App
		srvIns.Metadata["ipAddr"] = instance.IPAddr
		srvIns.Metadata["vipAddress"] = instance.VipAddress
		srvIns.Metadata["secureVipAddress"] = instance.SecureVipAddress
		srvIns.Metadata["status"] = instance.Status
		srvIns.Metadata["port"] = strconv.Itoa(instance.Port.Port)
		srvIns.Metadata["portEnable"] = instance.Port.Enabled
		srvIns.Metadata["securePort"] = strconv.Itoa(instance.SecurePort.Port)
		srvIns.Metadata["securePortEnable"] = instance.SecurePort.Enabled
		srvIns.Metadata["homePageUrl"] = instance.HomePageURL
		srvIns.Metadata["statusPageUrl"] = instance.StatusPageURL
		srvIns.Metadata["healthCheckUrl"] = instance.HealthCheckURL
		srvIns.Metadata["dataCenterName"] = instance.DataCenterInfo.Name
		srvIns.Metadata["dataCenterClass"] = instance.DataCenterInfo.Class
		srvIns.Metadata["leaseRenewalIntervalInSecs"] = strconv.Itoa(instance.LeaseInfo.RenewalIntervalInSecs)
		srvIns.Metadata["leaseDurationInSecs"] = strconv.Itoa(instance.LeaseInfo.DurationInSecs)
		srvIns.Metadata["leaseRegistrationTimestamp"] = strconv.FormatInt(instance.LeaseInfo.RegistrationTimestamp, 10)
		srvIns.Metadata["leaseLastRenewalTimestamp"] = strconv.FormatInt(instance.LeaseInfo.LastRenewalTimestamp, 10)
		srvIns.Metadata["leaseEvictionTimestamp"] = strconv.FormatInt(instance.LeaseInfo.EvictionTimestamp, 10)
		srvIns.Metadata["leaseServiceUpTimestamp"] = strconv.FormatInt(instance.LeaseInfo.ServiceUpTimestamp, 10)
		srvIns.Metadata["isCoordinatingDiscoveryServer"] = instance.IsCoordinatingDiscoveryServer

		if instance.Metadata["Endpoints"] != "" {
			srvIns.Endpoints = []string{instance.Metadata["Endpoints"]}
			items = append(items, srvIns)
		} else if instance.Metadata["Endpoints"] == "" {
			scheme := "http"
			if instance.Metadata["scheme"] != "" {
				scheme = instance.Metadata["scheme"]
			}
			host := instance.IPAddr
			if host == "" {
				host = instance.HostName
			}
			port := instance.Port.Port
			if instance.SecurePort.Enabled == "true" {
				port = instance.SecurePort.Port
				scheme = scheme + "s"
			}
			srvIns.Endpoints = []string{fmt.Sprintf("%s://%s:%d", scheme, host, port)}
			items = append(items, srvIns)
		}
	}
	return items
}

func parseInstance(service *registry.ServiceInstance) []Instance {
	res := make([]Instance, 0, len(service.Endpoints))
	for _, ep := range service.Endpoints {
		start := strings.Index(ep, "//")
		end := strings.LastIndex(ep, ":")
		appID := strings.ToUpper(service.Name)
		ip := ep[start+2 : end]
		sport := ep[end+1:]
		port, _ := strconv.Atoi(sport)
		portEnable := "true"
		securePort := 443
		securePortEnable := "false"
		hostName := service.ID
		homePageURL := fmt.Sprintf("%s/", ep)
		statusPageURL := fmt.Sprintf("%s/info", ep)
		healthCheckURL := fmt.Sprintf("%s/health", ep)
		status := statusUp
		leaseInfo := LeaseInfo{
			RenewalIntervalInSecs: 30,
			DurationInSecs:        90,
		}
		dataCenterInfo := DataCenterInfo{
			Name:  "MyOwn",
			Class: "com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo",
		}
		metadata := make(map[string]string)
		if len(service.Metadata) > 0 {
			metadata = service.Metadata
		}
		if s, ok := service.Metadata["hostName"]; ok {
			hostName = s
		}
		if s, ok := service.Metadata["securePort"]; ok {
			securePort, _ = strconv.Atoi(s)
		}
		if s, ok := service.Metadata["securePortEnable"]; ok {
			securePortEnable = s
		}
		if s, ok := service.Metadata["portEnable"]; ok {
			portEnable = s
		}
		if s, ok := service.Metadata["srvUP"]; ok {
			if s == "true" {
				status = statusUp
			} else {
				status = statusDown
			}
		}
		if s, ok := service.Metadata["homePageURL"]; ok {
			homePageURL = s
		}
		if s, ok := service.Metadata["statusPageURL"]; ok {
			statusPageURL = s
		}
		if s, ok := service.Metadata["healthCheckURL"]; ok {
			healthCheckURL = s
		}
		if s, ok := service.Metadata["leaseRenewalIntervalInSecs"]; ok {
			leaseInfo.RenewalIntervalInSecs, _ = strconv.Atoi(s)
		}
		if s, ok := service.Metadata["leaseDurationInSecs"]; ok {
			leaseInfo.DurationInSecs, _ = strconv.Atoi(s)
		}
		if s, ok := service.Metadata["leaseRegistrationTimestamp"]; ok {
			leaseInfo.RegistrationTimestamp, _ = strconv.ParseInt(s, 10, 64)
		}
		if s, ok := service.Metadata["leaseLastRenewalTimestamp"]; ok {
			leaseInfo.LastRenewalTimestamp, _ = strconv.ParseInt(s, 10, 64)
		}
		if s, ok := service.Metadata["leaseEvictionTimestamp"]; ok {
			leaseInfo.EvictionTimestamp, _ = strconv.ParseInt(s, 10, 64)
		}
		if s, ok := service.Metadata["leaseServiceUpTimestamp"]; ok {
			leaseInfo.ServiceUpTimestamp, _ = strconv.ParseInt(s, 10, 64)
		}
		if s, ok := service.Metadata["dataCenterName"]; ok {
			dataCenterInfo.Name = s
		}
		if s, ok := service.Metadata["dataCenterClass"]; ok {
			dataCenterInfo.Class = s
		}
		instanceID := strings.Join([]string{hostName, service.Name, sport}, ":")
		metadata["ID"] = service.ID
		metadata["Name"] = service.Name
		metadata["Version"] = service.Version
		metadata["Endpoints"] = ep
		metadata["agent"] = "go-eureka-client"
		res = append(res, Instance{
			InstanceID: instanceID,
			HostName:   hostName,
			App:        appID,
			IPAddr:     ip,
			Port: Port{
				Port:    port,
				Enabled: portEnable,
			},
			Status:           status,
			VipAddress:       service.Name,
			SecureVipAddress: service.Name,
			SecurePort: Port{
				Port:    securePort,
				Enabled: securePortEnable,
			},
			HomePageURL:    homePageURL,
			StatusPageURL:  statusPageURL,
			HealthCheckURL: healthCheckURL,
			DataCenterInfo: dataCenterInfo,
			LeaseInfo:      leaseInfo,
			Metadata:       metadata,
		})
	}

	return res
}
