package registry

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
	"fmt"
	"testing"
)

// pkcs7Unpadding 去填充函数
func pkcs7Unpadding(src []byte) []byte {
	length := len(src)
	unpadding := int(src[length-1])
	if unpadding < 1 || unpadding > length {
		panic("无效的填充")
	}
	return src[:(length - unpadding)]
}
func TestName(t *testing.T) {
	// 密钥，长度可以是16, 24, 32字节，分别对应AES-128, AES-192, AES-256
	key := []byte("8os7w9Er71pHYM8x")
	// 初始化向量，必须是块大小（16字节）
	iv := []byte("jCvDsFMx8luuYQVl")
	// 密文，假设这是已经通过AES CBC模式加密并使用PKCS7填充的数据
	ciphertext, _ := base64.StdEncoding.DecodeString("bQTSmWirSHjslw2AbVgUBQ==")
	// 创建 AES 解密器
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}
	// 检查密文长度是否为块大小的倍数
	//if len(ciphertext)%block.BlockSize() != 0 {
	//	panic("密文长度不是块大小的倍数")
	//}
	// 创建一个和密文长度相同的切片，用于存放解密后的数据
	mode := cipher.NewCBCDecrypter(block, iv)
	mode.CryptBlocks(ciphertext, ciphertext)
	// 去除填充
	plaintext := pkcs7Unpadding(ciphertext)
	// 将解密后的数据从字节切片转换为字符串
	fmt.Printf("解密后的文本: %s\n", plaintext)
}
