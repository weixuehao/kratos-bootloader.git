package discovery

import (
	"context"
	"encoding/json"
	"github.com/go-kratos/kratos/v2/errors"
	"net/url"
	"strings"
	"time"

	"gitee.com/weixuehao/kratos-bootloader/pkg/common/endpoint"
	"github.com/go-kratos/aegis/subset"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/go-kratos/kratos/v2/registry"
	"github.com/go-kratos/kratos/v2/selector"
)

// Target is resolver target
type Target struct {
	Scheme    string
	Authority string
	Endpoint  string
}

func parseTarget(endpoint string, insecure bool) (*Target, error) {
	if !strings.Contains(endpoint, "://") {
		if insecure {
			endpoint = "http://" + endpoint
		} else {
			endpoint = "https://" + endpoint
		}
	}
	u, err := url.Parse(endpoint)
	if err != nil {
		return nil, err
	}
	target := &Target{Scheme: u.Scheme, Authority: u.Host}
	if len(u.Path) > 1 {
		target.Endpoint = u.Path[1:]
	}
	return target, nil
}

type Resolver struct {
	ctx         context.Context
	cancel      context.CancelFunc
	rebalancer  selector.Selector
	nodeFilters []selector.NodeFilter
	target      *Target
	watcher     registry.Watcher
	selectorKey string
	subsetSize  int
	block       bool
	debugLog    bool
	insecure    bool
}

func (r *Resolver) watch() {
	if r.block {
		done := make(chan error, 1)
		go func() {
			for {
				select {
				case <-r.ctx.Done():
					done <- r.ctx.Err()
					return
				default:
				}
				services, err := r.watcher.Next()
				if err != nil {
					done <- err
					return
				}
				if r.update(services) {
					done <- nil
					return
				}
			}
		}()
		select {
		case err := <-done:
			if err != nil {
				stopErr := r.watcher.Stop()
				if stopErr != nil {
					log.Errorf("failed to http client watch stop: %v, error: %+v", r.target, stopErr)
				}
				return
			}
		case <-r.ctx.Done():
			log.Errorf("http client watch service %v reaching context deadline!", r.target)
			stopErr := r.watcher.Stop()
			if stopErr != nil {
				log.Errorf("failed to http client watch stop: %v, error: %+v", r.target, stopErr)
			}
			return
		}
	}
	go func() {
		for {
			select {
			case <-r.ctx.Done():
				return
			default:
			}
			services, err := r.watcher.Next()
			if err != nil {
				if errors.Is(err, context.Canceled) {
					return
				}
				log.Errorf("http client watch service %v got unexpected error:=%v", r.target, err)
				time.Sleep(time.Second)
				continue
			}
			r.update(services)
		}
	}()
}

func (r *Resolver) update(services []*registry.ServiceInstance) bool {
	filtered := make([]*registry.ServiceInstance, 0, len(services))
	for _, ins := range services {
		ept, err := endpoint.ParseEndpoint(ins.Endpoints, endpoint.Scheme(r.target.Scheme, !r.insecure))
		if err != nil {
			log.Errorf("Failed to parse (%v) discovery endpoint: %v error %v", r.target, ins.Endpoints, err)
			continue
		}
		if ept == "" {
			continue
		}
		filtered = append(filtered, ins)
	}
	if r.subsetSize != 0 {
		filtered = subset.Subset(r.selectorKey, filtered, r.subsetSize)
	}
	nodes := make([]selector.Node, 0, len(filtered))
	for _, ins := range filtered {
		ept, _ := endpoint.ParseEndpoint(ins.Endpoints, endpoint.Scheme(r.target.Scheme, !r.insecure))
		nodes = append(nodes, selector.NewNode(r.target.Scheme, ept, ins))
	}

	if len(nodes) == 0 {
		log.Warnf("[http resolver]Zero endpoint found,refused to write,set: %s ins: %v", r.target.Endpoint, nodes)
		return false
	}
	r.rebalancer.Apply(nodes)
	if r.debugLog {
		b, _ := json.Marshal(filtered)
		log.Debugf("[resolver] update instances: %s", b)
	}
	return true
}

// GetServiceNode return scheme of discovery
func (r *Resolver) GetServiceNode(ctx context.Context) (context.Context, selector.Node, error) {
	var done func(context.Context, selector.DoneInfo)
	var (
		err  error
		node selector.Node
	)
	var p selector.Peer
	ctx = selector.NewPeerContext(ctx, &p)
	if node, done, err = r.rebalancer.Select(ctx, selector.WithNodeFilter(r.nodeFilters...)); err != nil {
		return ctx, nil, errors.ServiceUnavailable("NODE_NOT_FOUND", err.Error())
	}

	if done != nil {
		done(ctx, selector.DoneInfo{Err: err})
	}

	if r.debugLog {
		b, _ := json.Marshal(node)
		log.Debugf("[rebalancer] update node: %s", b)
	}
	return ctx, node, nil
}

func (r *Resolver) Close() error {
	r.cancel()
	return r.watcher.Stop()
}
