package discovery

import (
	"context"
	"errors"
	"github.com/go-kratos/kratos/v2/registry"
	"github.com/go-kratos/kratos/v2/selector"
	"github.com/go-kratos/kratos/v2/selector/wrr"
	"github.com/google/uuid"
	"time"
)

const name = "discovery"

var ErrWatcherCreateTimeout = errors.New("discovery create watcher overtime")

func init() {
	if selector.GlobalSelector() == nil {
		selector.SetGlobalSelector(wrr.NewBuilder())
	}
}

// Option is builder option.
type Option func(o *Builder)

// WithTimeout with timeout option.
func WithTimeout(timeout time.Duration) Option {
	return func(b *Builder) {
		b.timeout = timeout
	}
}

// WithInsecure with isSecure option.
func WithInsecure(insecure bool) Option {
	return func(b *Builder) {
		b.insecure = insecure
	}
}

// WithSubset with subset size.
func WithSubset(size int) Option {
	return func(b *Builder) {
		b.subsetSize = size
	}
}

// Deprecated: please use PrintDebugLog
// DisableDebugLog disables update instances log.
func DisableDebugLog() Option {
	return func(b *Builder) {
		b.debugLog = false
	}
}

// PrintDebugLog print grpc resolver watch service log
func PrintDebugLog(p bool) Option {
	return func(b *Builder) {
		b.debugLog = p
	}
}

// WithNodeFilter with select filters
func WithNodeFilter(filters ...selector.NodeFilter) Option {
	return func(o *Builder) {
		o.nodeFilters = filters
	}
}

// WithBlock with client block.
func WithBlock() Option {
	return func(o *Builder) {
		o.block = true
	}
}


type Builder struct {
	discoverer registry.Discovery
	nodeFilters []selector.NodeFilter
	timeout    time.Duration
	insecure   bool
	subsetSize int
	debugLog   bool
	block      bool
}

// NewResolverBuilder creates a builder which is used to factory registry resolvers.
func NewResolverBuilder(endpoint string, d registry.Discovery, opts ...Option) (*Resolver, error) {
	b := &Builder{
		discoverer: d,
		timeout:    time.Second * 10,
		insecure:   false,
		debugLog:   true,
		subsetSize: 25,
	}
	for _, o := range opts {
		o(b)
	}
	return b.build(endpoint)
}

func (b *Builder) build(endpoint string) (*Resolver, error) {
	target, err2 := parseTarget(endpoint,b.insecure)
	if err2 != nil {
		return nil,err2
	}

	watchRes := &struct {
		err error
		w   registry.Watcher
	}{}

	done := make(chan struct{}, 1)
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		w, err := b.discoverer.Watch(ctx, target.Endpoint)
		watchRes.w = w
		watchRes.err = err
		close(done)
	}()

	var err error
	select {
	case <-done:
		err = watchRes.err
	case <-time.After(b.timeout):
		err = ErrWatcherCreateTimeout
	}
	if err != nil {
		cancel()
		return nil, err
	}

	selector := selector.GlobalSelector().Build()

	r := &Resolver{
		rebalancer: selector,
		nodeFilters: b.nodeFilters,
		watcher:     watchRes.w,
		ctx:         ctx,
		cancel:      cancel,
		insecure:    b.insecure,
		debugLog:    b.debugLog,
		subsetSize:  b.subsetSize,
		selectorKey: uuid.New().String(),
		block: b.block,
	}
	go r.watch()
	return r, nil
}


//// Scheme return scheme of discovery
//func (*Builder) Scheme() string {
//	return name
//}
